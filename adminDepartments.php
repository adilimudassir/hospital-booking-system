<?php
if(isset($_GET["del"])){
	if(Query("DELETE FROM departments WHERE dept_id = ".$_GET["del"]."")){
		$message = "Department deleted";
	}
}
?>
<div class="panel panel-primary">
	<?php if(!isset($_GET["edit"])){ ?>
		<div class="panel-heading">Manage Departments</div>
	<div class="panel-body">
		<div class="row" style="margin:5px;">
		<div class="col-sm-9">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
		</div>
		<div class="col-sm-3">
			<a class="btn btn-primary pull-right" href="<?=WEB_ROOT;?>view.php?mod=admin&view=AddDept">Add new Department</a>
		</div>
	</div>
	<div class="panel-body">  
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No.</th>
					<th>Name</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
		<?php 
		$sql = Query("SELECT * FROM  departments");
		$n = 0;
		while($row1 = fetchAssoc($sql)){?>
				<tr>
					<td><?=++$n; ?></td>
					<td><?=$row1["dept_name"]; ?></td>
					<td> <div align="center">
					
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
							<span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Departments&del=<?=$row1["dept_id"]; ?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
									</li>
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Departments&edit=<?=$row1["dept_id"]; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
									</li>
							</ul>
						</div>
					

			</div></td>
					
				</tr>
	<?php } ?>
			</tbody>
		</table>
		
	</div>
	<?php } else { 

		$id = $_GET["edit"];


		if(isset($_POST["updateDept"])){

			$name = $_POST["name"];
	    	$sql = Query("UPDATE `departments` SET `dept_name`='$name' WHERE `dept_id` ='$id'");
				if($sql){
					$message = "Department Updated";
				}else{
					$message = "Operation failed";
				}
		}
		
		$sql = Query("SELECT * FROM departments WHERE dept_id ='$id'");
		$row = fetchAssoc($sql);

		?>

	<div class="panel-heading">Edit Department</div>
	<div class="panel-body">
		<?php if(isset($message) and $message != ""){ ?>
        <div class="alert alert-info">
             <strong><?=$message; ?></strong>
        </div>
    <?php }  ?>
        <form class="form-horizontal" method="POST" action="">
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Name</label>
                <div class="col-md-10">
                    <input id="name" type="text" class="form-control" value="<?=$row["dept_name"];?>" name="name" required autofocus />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <input type="submit" name="updateDept" value="Update" class="btn btn-success" />
                </div>
            </div>
        </form>
	</div>
	<?php } ?>
</div> 			