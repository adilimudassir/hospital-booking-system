<?php
if(isset($_POST["submit"])){
        
    $sql = Query("SELECT MAX(docid) FROM doctor");
    $row = fetchArray($sql);
    
    $id = $row[0];
    $id++;
        
    $name = $_POST["name"];
    $dept = $_POST["department"];
    $spec = $_POST["speciality"];
    $password = $_POST["password"];
    $cpassword = $_POST["cpassword"];
    $email = $_POST["email"];
    $tel = $_POST["tel"];
    $sex = $_POST["sex"];
    
   if(!preg_match("/^[a-zA-Z ]*$/",$name)){
        $message = "Name can only contain characters";
    } else {
        $sql = Query("INSERT INTO doctor (
                                                docid,
                                                name,
                                                speciality,
                                                password,
                                                email,
                                                tel,
                                                department,
                                                sex,
                                                date
                                                )
                                        VALUES (
                                                '$id',
                                                '$name',
                                                '$spec',
                                                '$password',
                                                '$email',
                                                '$tel',
                                                '$dept',
                                                '$sex',
                                                NOW()
                                                )");

        if($sql){
            $message = "Added. The Doctor ID is: ".$id;
        } else {
            "Operation Failed";
        }
    
}
}
?>

<div class="panel panel-primary">
    <div class="panel-heading">Add New Doctor</div>

    <div class="panel-body">
    <?php if(isset($message) and $message != ""){ ?>
        <div class="alert alert-info">
             <strong><?=$message; ?></strong>
        </div>
    <?php }  ?>
        <form class="form-horizontal" method="POST" action="" id="form" novalidate="novalidate">
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Name</label>
                <div class="col-md-10">
                    <input id="name" type="text" class="form-control" name="name" required autofocus />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Specialty</label>
                <div class="col-md-10">
                    <input id="speciality" type="text" class="form-control" name="speciality" required/>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-md-2 control-label">Department</label>
                <div class="col-md-10">
                    <select class="form-control" name="department" id="department">
                        <option value="">-----Select----</option>   
                            <?php
                            $sql1 = Query("SELECT * FROM departments");
                            while($doc = fetchAssoc($sql1)){
                            ?>
                            <option value="<?=$doc["dept_id"]; ?>"><?=$doc["dept_name"]; ?></option>
                            <?php }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-md-2 control-label">E-Mail Address</label>
                <div class="col-md-10">
                    <input id="email" type="email" class="form-control" name="email" value="" required />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Sex</label>
                <div class="col-md-10">
                   <select required class="form-control" name="sex" id="sex">
                        <option value="">----SELECT----</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Phone Number</label>
                <div class="col-md-10">
                    <input id="tel" type="text" class="form-control" name="tel" required/>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-md-2 control-label">Password</label>
                <div class="col-md-10">
                    <input id="password" type="password" class="form-control" name="password" required>
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-2 control-label">Confirm Password</label>

                <div class="col-md-10">
                    <input id="cpassword" type="password" class="form-control" name="cpassword" required>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary" />
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                name : {
                   required  : true,
                   minlength : 3
                },
                speciality : {
                    required : true,
                    minlength : 3
                },
                department : {
                    required : true
                },
                email : {
                    required : true,
                    email    : true
                },
                sex : {
                    required : true
                },
                tel : {
                    required : true,
                    digits   : true,
                    minlength : 11,
                    maxlength : 11
                },
                password : {
                    required : true,
                    minlength : 6
                },
                cpassword : {
                    required : true,
                    equalTo  : '#password'
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>

