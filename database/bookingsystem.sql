-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 11, 2020 at 11:44 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookingsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(1, 'sirhaziq', 'sirhaziq@gmail.com', '123');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `patid` int(11) NOT NULL,
  `docid` int(11) NOT NULL,
  `atime` varchar(10) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `adate` date NOT NULL,
  `status` text NOT NULL,
  `comment` varchar(255) NOT NULL,
  `currentdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `happen` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `patid`, `docid`, `atime`, `date`, `adate`, `status`, `comment`, `currentdate`, `happen`) VALUES
(15, 1005, 2002, '11:15', '2017-04-06 15:25:45', '2017-04-09', '', '', '0000-00-00 00:00:00', ''),
(27, 1009, 2003, '05:45', '2017-04-07 01:44:17', '2017-04-12', 'pending', '2;lkel;', '0000-00-00 00:00:00', ''),
(31, 1001, 2003, '01:00 PM', '2017-04-07 02:40:32', '2017-04-16', 'pending', 'Very Sick', '0000-00-00 00:00:00', ''),
(33, 1021, 2002, '11:30 AM', '2017-04-07 16:50:20', '2017-08-15', 'pending', 'i need a speininological cardiac sugery', '0000-00-00 00:00:00', ''),
(52, 1013, 2007, '11:30 AM', '2017-04-20 07:08:01', '2017-04-21', 'pending', 'sick man', '2017-04-20 10:58:45', ''),
(58, 1013, 2002, '11:30 AM', '2017-04-19 11:14:09', '2017-04-27', 'pending', 'lkqwhdoic', '2017-04-19 11:14:15', ''),
(59, 1013, 2001, '10:45 AM', '2017-04-19 12:02:28', '2017-04-19', 'pending', 'slkckkw', '2017-04-19 12:02:34', ''),
(60, 1013, 2004, '06:15 PM', '2017-04-20 06:43:25', '2017-04-22', 'pending', 'me', '2017-04-20 06:43:30', ''),
(62, 1013, 2007, '07:30 PM', '2017-04-20 07:10:28', '2017-04-21', 'pending', 'jhkl;', '2017-04-20 07:10:33', ''),
(63, 1013, 2003, '01:15 PM', '2017-04-20 08:23:53', '2017-04-20', 'pending', 'asdfc', '2017-04-20 08:24:01', ''),
(64, 1013, 2002, '11:45 AM', '2017-04-20 10:42:34', '2017-04-20', 'pending', 'am sickoo', '2017-04-20 10:42:41', ''),
(65, 1001, 2002, '11:45 AM', '2017-04-20 11:17:52', '2017-04-22', 'pending', 'sick', '2017-04-20 11:17:58', ''),
(66, 1022, 2001, '10:00 AM', '2017-04-20 13:25:52', '2017-04-24', 'pending', 'caugh', '2017-04-20 13:26:07', ''),
(67, 1022, 2002, '11:15 AM', '2017-04-20 13:28:49', '2017-04-21', 'pending', 'eye', '2017-04-20 13:29:13', ''),
(68, 1023, 2002, '11:45 AM', '2017-04-26 09:46:09', '2017-04-27', 'pending', '45gf54f4', '2017-04-26 09:46:35', ''),
(69, 1024, 2001, '11:30 AM', '2019-12-07 13:04:34', '2019-12-12', 'pending', 'sadasdasd', '2019-12-07 13:04:34', 'no'),
(70, 1013, 2007, '03:45 PM', '2019-12-07 13:24:09', '2019-12-11', 'pending', 'saDAA', '2019-12-07 13:24:09', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `dept_id` int(11) NOT NULL,
  `dept_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dept_id`, `dept_name`) VALUES
(1, 'Pediatricsr');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `docid` int(11) NOT NULL,
  `name` text NOT NULL,
  `sex` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `department` text NOT NULL,
  `speciality` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `date` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`docid`, `name`, `sex`, `password`, `department`, `speciality`, `email`, `tel`, `date`) VALUES
(2001, 'Ahmad S Adili', '', '123', 'Cardiology', 'Cardiologist', 'ahmad@gmail.com', '09033068587', ''),
(2002, 'Ahmad Adili', '', 'qwe', 'Cardiology', 'Cardiologist', 'ahmad@gmail.com', '09033068587', ''),
(2003, 'Ahmad Adili', '', 'qwe', 'Cardiology', 'Cardiologist', 'ahmad@gmail.com', '09033068587', ''),
(2004, 'Ahmad Adili', '', 'qwe', 'Cardiology', 'Cardiologist', 'ahmad@gmail.com', '09033068587', ''),
(2005, 'Ahmad Adili', '', 'qwe', 'Cardiology', 'Cardiologist', 'ahmad@gmail.com', '09033068587', ''),
(2006, 'Ahmad Adili', '', 'qwe', 'Cardiology', 'Cardiologist', 'ahmad@gmail.com', '09033068587', ''),
(2007, 'Ben Carson', '', '123', 'Neurology', 'Neuro', 'bem@gmail.com', '09033068587', ''),
(2008, 'Mahmud Bakale', 'Male', '123456', '1', 'Software Engineerings', 'bakale@xmail.com', '08038969767', '');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `patid` int(11) NOT NULL DEFAULT '1000',
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` int(11) NOT NULL,
  `sex` text NOT NULL,
  `department` text NOT NULL,
  `date` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patid`, `fname`, `lname`, `pwd`, `email`, `tel`, `sex`, `department`, `date`) VALUES
(1001, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, 'Female', '1', ''),
(1002, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1003, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1004, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1005, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1006, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1007, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1008, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1009, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1010, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1011, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1012, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1013, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1014, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1015, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1016, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1017, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1018, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1019, 'Mudassir', 'Adili', '123', 'mudassiradili@gmail.com', 2147483647, '', '1', ''),
(1020, 'Isah ', 'Ladan', '111', 'isah@some.com', 2147483647, '', '1', ''),
(1021, 'musab', 'hassan', '12345', 'bodinga@gmail.com', 2147483647, '', '1', ''),
(1022, 'nabila', 'aminu', '1234', 'nabibawa03@gmail.com', 2147483647, '', '1', ''),
(1023, 'Mudassir', 'Adili', 'dogarawa', 'super-admin@iuoledu.net', 2147483647, 'Male', '1', ''),
(1024, 'Mudassir', 'Adili', 'dogarawa', 'super-admin@iuoledu.net', 2147483647, 'Male', '1', ''),
(1025, 'Maryam', 'Ahmad', 'mary12', 'maryam@gmail.com', 2147483647, 'Female', '1', ''),
(1026, 'Sadiya ', 'Anka', '123456', 'sadiya@gmail.com', 2147483647, 'Male', '1', ''),
(1027, 'Sadiya ', 'Anka', '123456', 'sadiya@gmail.com', 2147483647, 'Male', '1', ''),
(1028, 'Sadiya ', 'Anka', '123456', 'sadiya@gmail.com', 2147483647, 'Male', '1', ''),
(1029, 'Sadiya ', 'Anka', '123456', 'sadiya@gmail.com', 2147483647, 'Male', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `pm`
--

CREATE TABLE `pm` (
  `id` bigint(20) NOT NULL,
  `subject` varchar(256) NOT NULL,
  `sender` bigint(20) NOT NULL,
  `recipient` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hasRead` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pm`
--

INSERT INTO `pm` (`id`, `subject`, `sender`, `recipient`, `message`, `timestamp`, `hasRead`) VALUES
(1, 'Booking Appointment ', 2001, 1013, 'A computer is an electronic machine, operating under the control of instructions stored in its own memory, that can accept data, manipulate the data according to specified rules, produce results, and store the results for future use. Computers process data to create information. Data is a collection of raw unprocessed facts, figures, and symbols. Information is data that is organized, meaningful, and useful. To process data into information, a computer uses hardware and software. Hardware is the electric, electronic, and mechanical equipment that makes up a computer. Software is the series of instructions that tells the hardware how to perform tasks.', '2017-04-07 14:30:53', 'yes'),
(2, 'Appointment Reshecduling', 2005, 1013, 'A computer is an electronic machine, operating under the control of instructions stored in its own memory, that can accept data, manipulate the data according to specified rules, produce results, and store the results for future use. Computers process data to create information. Data is a collection of raw unprocessed facts, figures, and symbols. Information is data that is organized, meaningful, and useful. To process data into information, a computer uses hardware and software. Hardware is the electric, electronic, and mechanical equipment that makes up a computer. Software is the series of instructions that tells the hardware how to perform tasks.', '2017-04-07 14:56:07', 'yes'),
(3, 'Emergency', 2003, 1013, 'A computer is an electronic machine, operating under the control of instructions stored in its own memory, that can accept data, manipulate the data according to specified rules, produce results, and store the results for future use. Computers process data to create information. Data is a collection of raw unprocessed facts, figures, and symbols. Information is data that is organized, meaningful, and useful. To process data into information, a computer uses hardware and software. Hardware is the electric, electronic, and mechanical equipment that makes up a computer. Software is the series of instructions that tells the hardware how to perform tasks.', '2017-04-07 15:28:17', 'yes'),
(4, 'Booking Appointment ', 2001, 1013, 'A computer is an electronic machine, operating under the control of instructions stored in its own memory, that can accept data, manipulate the data according to specified rules, produce results, and store the results for future use. Computers process data to create information. Data is a collection of raw unprocessed facts, figures, and symbols. Information is data that is organized, meaningful, and useful. To process data into information, a computer uses hardware and software. Hardware is the electric, electronic, and mechanical equipment that makes up a computer. Software is the series of instructions that tells the hardware how to perform tasks.', '2017-04-09 14:30:53', 'yes'),
(5, 'I am In', 0, 1013, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2017-04-10 01:16:03', 'yes'),
(6, 'I am In', 2007, 1013, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2017-04-10 01:16:30', 'yes'),
(7, 'swdfghjgfdsa', 2007, 1013, 'dsfrtkljhgfdsfgh', '2017-04-10 01:17:03', 'yes'),
(8, 'I am In', 2007, 1013, 'd;wfnkjvredsjvcnokemd,.svmn jk,d.fgvn jkrmfd,j.znfc ofkm ', '2017-04-10 01:27:39', 'yes'),
(9, 'ijkhugt7y6t67r76r657r65r6', 2007, 1013, 'iuygfdjopikjl', '2017-04-10 10:31:35', 'yes'),
(10, 'Time extension', 1013, 2007, 'Please i need my time to be extended because i am not free on that day', '2017-04-18 08:14:49', 'yes'),
(11, '', 1013, 2002, '', '2017-04-18 13:55:23', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `timings`
--

CREATE TABLE `timings` (
  `timing_id` int(11) NOT NULL,
  `docid` bigint(20) NOT NULL,
  `day` varchar(60) NOT NULL,
  `timings` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timings`
--

INSERT INTO `timings` (`timing_id`, `docid`, `day`, `timings`) VALUES
(1, 2008, 'Monday', '09:00,21:45'),
(2, 2008, 'Tuesday', '11:30,16:00'),
(3, 2008, 'Wednesday', '14:00,17:00');

-- --------------------------------------------------------

--
-- Table structure for table `treatment`
--

CREATE TABLE `treatment` (
  `id` int(11) NOT NULL,
  `appt_id` int(11) NOT NULL,
  `patid` int(11) NOT NULL,
  `docid` int(11) NOT NULL,
  `disease` text NOT NULL,
  `treatment` text NOT NULL,
  `note` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dosage` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treatment`
--

INSERT INTO `treatment` (`id`, `appt_id`, `patid`, `docid`, `disease`, `treatment`, `note`, `date`, `dosage`) VALUES
(1, 52, 1013, 2007, 'Heart', 'Pills', 'Normal', '2017-04-19 08:28:52', '1-1-1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`docid`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`patid`);

--
-- Indexes for table `pm`
--
ALTER TABLE `pm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timings`
--
ALTER TABLE `timings`
  ADD PRIMARY KEY (`timing_id`);

--
-- Indexes for table `treatment`
--
ALTER TABLE `treatment`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pm`
--
ALTER TABLE `pm`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `timings`
--
ALTER TABLE `timings`
  MODIFY `timing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `treatment`
--
ALTER TABLE `treatment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
    