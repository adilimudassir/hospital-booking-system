<?php
if(isset($_POST["submit"])){
	$id = $_SESSION["patid"];
	
	$sql = Query("SELECT * FROM patient WHERE patid='$id'");
	$row = fetchAssoc($sql);
		
	$opwd = $_POST["opwd"];
	$npwd = $_POST["npwd"];
	$cpwd = $_POST["cpwd"];
	$id   = $_SESSION["patid"];

		if ($opwd == $row["pwd"]){
			if($npwd == $cpwd){
				$sql = Query("UPDATE patient SET pwd='$npwd' WHERE patid='$id'");
				if($sql){
					$message = "Password Changed";
				}else{
					$message = "Operation Failed";
				}
			}else{
				$message = "The new password did not match";
			}
		}else{
			$message= "The Old password is incorrect";
		}
}
	$id = $_SESSION["patid"];
	
	$sql = Query("SELECT * FROM patient WHERE patid='$id'");
	$row = fetchAssoc($sql);
?>
<div class="panel panel-primary">
	<div class="panel-heading">Change Password</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?=$message;?></strong>
		</div>
		<?php } ?>
		<form role="form" method="post" action="" id="form" novalidate="novalidate">
			<div class="form-group">
			<div class="form-group">
				<label for="pwd">Old Password:</label>
				<input type="password" class="form-control" id="opwd" name="opwd" placeholder="Enter password">
			</div>
				<label for="pwd"> New Password:</label>
				<input type="password" class="form-control" id="pwd" name="npwd" placeholder="Enter password">
			</div>
			<div class="form-group">
				<label for="pwd">Confirm Password:</label>
				<input type="password" class="form-control" id="cpwd" name="cpwd" placeholder="Confirm password">
			</div>
			<input type="submit" name="submit" class="btn btn-success" value="Submit" style="float:left;"> 
		</form>
	</div>
</div> 

<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                
                opwd : {
                    required : true,
                    minlength : 6
                },
                npwd : {
                    required : true,
                    minlength : 6
                },
                cpwd : {
                    required : true,
                    equalTo  : '#password'
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>			