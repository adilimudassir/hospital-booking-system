<?php
// ini_set('display_errors', 'off');
//ob_start("ob_gzhandler");
//error_reporting(E_ALL);


// DB
defined("HOST") or define('HOST','127.0.0.1');
defined("USERNAME") or define('USERNAME', 'root');
defined("PASSWORD") or define('PASSWORD','');
defined("DB") or define('DB','bookingsystem');


// setting up the web root and server root
$thisFile = str_replace('\\', '/', __FILE__);
$docRoot = $_SERVER['DOCUMENT_ROOT'];

$webRoot  = str_replace(array($docRoot, 'library/config.php'), '', $thisFile);
$srvRoot  = str_replace('library/config.php', '', $thisFile);

defined("WEB_ROOT") or define('WEB_ROOT', $webRoot);
defined("SRV_ROOT") or define('SRV_ROOT', $srvRoot);

// if (!get_magic_quotes_gpc()) {
// 	if (isset($_POST)) {
// 		foreach ($_POST as $key => $value) {
// 			$_POST[$key] =  trim(addslashes($value));
// 		}
// 	}
	
// 	if (isset($_GET)) {
// 		foreach ($_GET as $key => $value) {
// 			$_GET[$key] = trim(addslashes($value));
// 		}
// 	}	
// }

// since all page will require a database access
// and the common library is also used by all
// it's logical to load these library here
require_once 'database.php';

// get the shop configuration ( name, addres, etc ), all page need it
//$shopConfig = getShopConfig();


// start the session
session_start();