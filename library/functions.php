<?php
//FUNCTIONS REPOSITORY

///FORM PROTECTION
function cleanValues($string)
{
    $string = trim($string);
    $string = htmlspecialchars($string);
    if (get_magic_quotes_gpc()) {
        $string = stripslashes($string);
    }
    //$string = mysqli_real_escape_string($string);
    return $string;
}

//FILE SIZE
function GetSize($size)
{
    if ($size<1024) {
        $size = $size."B";
    } elseif ($size>1024 and $size<148576) {
        $size = round($size/1024, 1)."KB";
    } elseif ($size>148576 and $size<1073741824) {
        $size = round(($size/1024)/1024, 1)."MB";
    }
    return $size;
}

//DATE
function cDate($date)
{
    $cdate = date("M d", strtotime($date));
        
    return $cdate;
}


function Login($id, $pwd)
{
    $error = "";
    // Login
    if (isset($id) && isset($pwd)) {
        if (substr($id, 0, 1) == "1") {
            $sql = Query("SELECT * FROM patient WHERE patid = '$id' AND pwd = '$pwd'");
        
        
            if (numRows($sql) == 1) {
                $row = fetchAssoc($sql);
                
                session_start();
                $_SESSION["patid"] = $row["patid"];
                $_SESSION["name"] = $row["fname"]." ".$row["lname"];
                $_SESSION["user_type"] = "patient";
                $_SESSION["tel"] = $row["tel"];

                
                header("location:".WEB_ROOT."view.php?mod=patient&view=Home");
            } else {
                $error = "Incorrect ID or Password";
            }
        } elseif (substr($id, 0, 1) == "2") {
            $sql = Query("SELECT * FROM doctor WHERE docid = '$id' AND password = '$pwd'");
        
        
            if (numRows($sql) == 1) {
                $row = fetchAssoc($sql);
                
                session_start();
                $_SESSION["id"] = $row["docid"];
                $_SESSION["name"] = $row["name"];
                ;
                $_SESSION["user_type"] = "doctor";

                
                header("location:".WEB_ROOT."view.php?mod=doctor&view=Home");
            } else {
                $error = "Incorrect ID or Password";
            }
        } else {
            $sql = Query("SELECT * FROM admin WHERE name = '$id' AND password = '$pwd'");
    
    
            if (numRows($sql) == 1) {
                $row = fetchAssoc($sql);
                
                session_start();
                $_SESSION["id"] = $row["id"];
                $_SESSION["name"] = $row["name"];
                $_SESSION["user_type"] = "admin";

                
                header("location:".WEB_ROOT."view.php?mod=admin&view=Home");
            } else {
                $error = "Incorrect Username Or Password";
            }
        }
    }
    return $error;
}

function checkUser()
{
    // if the session id is not set, redirect to login page
    if (!isset($_SESSION['user_type'])) {
        header('Location: '. WEB_ROOT .'login.php');
        exit;
    }
    
    // the user want to logout
    if (isset($_GET['logOut'])) {
        doLogout();
    }
}

/*
    Logout a user
*/
function doLogout()
{
    if (isset($_SESSION['patid'])) {
        unset($_SESSION['patid']);
    }
    if (isset($_SESSION['id'])) {
        unset($_SESSION['id']);
    }
    if (isset($_SESSION['name'])) {
        unset($_SESSION['name']);
    }
    if (isset($_SESSION['user_type'])) {
        unset($_SESSION['user_type']);
    }
    session_destroy();
    header('Location:login.php');
    exit;
}


function sms($phone, $message)
{
    $url  = "http://smsclone.com/components/com_spc/smsapi.php";
    $url .= "?username=nabilabawa";
    $url .= "&password=1210310142";
    $url .= "&sender=BOOKINGSYSTEM";
    $url .= "&recipient='$phone'";
    $url .= "&message='$message'";
    $fp   = @fopen($url, "r", 255);
    return $fp;
}

// sms('07023413123', 'COnrasfjsadajsh');
function getTime($time)
{
    $timestamp = strtotime($time);
    $finaltime = date("h:i A",$timestamp);
    return $finaltime;

}
