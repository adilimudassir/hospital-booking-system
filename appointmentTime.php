 <?php
require_once './library/config.php';
require_once './library/functions.php';

$appointmentDate = $_GET["adate"];


$timestamp = strtotime($_GET['adate']);

$day = date('l', $timestamp);
//var_dump($day);

if(isset($_GET["id"],$_GET["name"],$_GET["department"])){
	$_SESSION["appDocID"] = $_GET["id"]; 
	$_SESSION["appDocName"] = $_GET["name"];
	$_SESSION["appDocDept"] = $_GET["department"];
	header("Location:".WEB_ROOT."makeAppointment.php");
}

$patid = $_SESSION["patid"];
$docid = $_SESSION["appDocID"];

$sql = Query("SELECT * FROM appointment WHERE patid = '$patid' AND docid = '$docid' AND adate = '$appointmentDate'"); 

$appointmentsTaken = numRows($sql);

$sql = Query("SELECT * FROM timings WHERE docid = '$docid' AND day ='$day'");

$row = fetchAssoc($sql);

$timeSlots = explode(",", $row["timings"]);

$availableTimeSlots = array();

$doctorAppointments = Query("SELECT * FROM appointment WHERE docid = '$docid' AND adate = '$appointmentDate'");
    
if (numRows($doctorAppointments) == 0) {
	$availableTimeSlots = $timeSlots;
}

if (numRows($doctorAppointments) > 0) {
	while ($row = fetchAssoc($doctorAppointments)) {
		for ($i=0; $i < count($timeSlots); $i++) {
			if ($row["atime"] != $timeSlots[$i]) {
				$availableTimeSlots[$i] = $timeSlots[$i];
			}
		}
	}
}

if (isset($_POST["submit"])) {
    $appointmentsTakenRecord = Query("SELECT MAX(id) FROM appointment");
    $row = fetchArray($appointmentsTakenRecord);
    $id = $row[0];
    $timeSlot = getTime($_POST["time"]);
    
    $check = Query("SELECT * FROM appointment WHERE docid = '$docid' AND adate = '$appointmentDate' AND atime='$timeSlot'");
    // $me = numRows($check);
    if (numRows($check) > 0) {
        $message = "Sorry, This time slot is already taken";
    } else {

    	 $timeFrom = getTime($timeSlots[0]);
    	 $timeTo   = getTime($timeSlots[1]);

    	if (($timeSlot < $timeFrom) || ($timeSlot > $timeTo)) {
    		$message = "wrong time cannot be less or greater than the available timing";
    	}else{
    		if (!empty($timeSlot)) {
    					$comment = $_SESSION["comment"];
        $sql = Query("INSERT INTO appointment (`patid`,
												`docid`,
												`adate`,
												`comment`,
												`atime`,
												`status`,
												`happen`
												) VALUES (
												'$patid',
												'$docid',
												'$appointmentDate',
												'$comment',
												'$timeSlot',
												'pending',
												'no'
												)"
				);
        
        if ($sql) {
			$message = "Saved"; 
			?>
				
			<script
				src="https://code.jquery.com/jquery-3.2.1.min.js"
				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
				crossorigin="anonymous"
			></script>
			<script>

			$.get(
				"http://smsclone.com/components/com_spc/smsapi.php?username=nabilabawa&password=1210310142&sender=UDUTH&recipient='<?=$_SESSION["tel"]; ?>'&message='You have successfully booked appointment with <?=$_SESSION["appDocName"]; ?> On <?=$appointmentDate; ?> by <?=$timeSlot; ?>'"
			)
			</script>
			
			<?php
            	header("Location:".WEB_ROOT."view.php?mod=patient&view=ViewAppointments");
        } else {
            $message = "Failed";
        }
    		}
    	}


    }
}


$today = date("Y-m-d");

require_once 'include/header.php';
require_once 'include/nav.php';

?>
<div class="row">
    
    <div class="col-sm-3">
      <?php require 'include/menu.php'; ?>
    </div>
    <div class="col-sm-9">
      <div class="panel panel-primary">

	<div class="panel-heading">Appointment Date: <?=$appointmentDate;?></div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?=$message;?></strong>
		</div>
		<?php } ?>
		<?php if($appointmentsTaken  == 0 ){ ?>
		 <h3 class="text-primary">Available Appointment Time is From <?php 	echo getTime($timeSlots[0]); ?> To <?php echo getTime($timeSlots[1]); ?></h3>
		 <hr>   
			<form role="form" method="post" action="#">
				<div class="form-group">
					<label for="time">Choose Time From <?php echo getTime($timeSlots[0]); ?> To <?php echo getTime($timeSlots[1]); ?></label>
					<input type="time" name="time" class="form-control">
				</div>
				<input type="submit" name="submit" style="margin:auto 40%;" id="submit" class="btn btn-primary" value="Save" />
			</form>
			
		<?php 
		} else {
	    ?>
			<div class="alert alert-info">
			  <strong>Sorry!!</strong> you can't take more than one appointment for the same doctor on a day
			</div>
			<a href="<?=WEB_ROOT;?>view.php?mod=patient&view=BookAppointment" class="btn btn-primary pull-right">< Back</a>
		<?php 
			}
		 ?>
	</div>
</div> 			
    </div>

</div>

<?php

require_once 'include/footer.php'; 
?>
