<?php
if(isset($_GET["del"])){
	if(Query("DELETE FROM appointment WHERE id = ".$_GET["del"]."")){
		$message = "Appointment Deleted";
	}
}
?>
<div class="panel panel-primary">
	
	<?php if(!isset($_GET["edit"])){ ?>
	<div class="panel-heading">Manage Appointments</div>
	<div class="panel-body">
		<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Appointment<br />Date</th>
						<th>Appointment <br /> Time</th>
						<th>Doctor Name</th>
						<th>Patient Name</th>
						<th>Complaint</th>
						<th>Status</th>
						<th>Options</th>
					</tr>
				</thead>
	
				<tbody>
					<?php 
						$sql = Query("SELECT * FROM  appointment");
						$n = 0;
						while($row1 = fetchArray($sql)){ ?>
					<tr>
						<td><?=++$n; ?></td>
						<td><?=date("d-m-Y", strtotime($row1["adate"])); ?></td>
						<td><?=$row1["atime"]; ?></td>
						<td><?php

						$result= Query("SELECT * FROM doctor WHERE docid=".$row1["docid"]."");
						$row = fetchArray($result);
							echo $row["name"];
						?></td>
						<td><?php

						$result= Query("SELECT * FROM patient WHERE patid=".$row1["patid"]."");
						$row = fetchArray($result);
							echo $row["fname"]." ".$row["lname"];
						?></td>
						<td><?=$row1["comment"]; ?></td>
						<td><?=$row1["status"]; ?></td>
						<td> 
							<div align="center">
						
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li>
											<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Appointments&edit=<?=$row1["id"]; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
										</li>
										<li>
											<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Appointments&del=<?=$row1["id"]; ?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
										</li>
									</ul>
								</div>
						

							</div>
						</td>
						
					</tr>
					<?php } ?>
				</tbody>
			</table>
	</div>
	<?php  } else {	
	
	$id = $_SESSION["id"];	
	$appt = $_GET["edit"];
	$sql = Query("SELECT * FROM  appointment WHERE id = $appt");
	$row = fetchAssoc($sql);
	if(isset($_POST["updateAppointment"])){
		$date = $_POST["adate"];
		$time = $_POST["atime"];		
		$status = $_POST["status"];
		$comment = $_POST["comment"];

		$sql = Query("UPDATE appointment SET adate = '$date',  atime = '$time', status = '$status', comment = '$comment' WHERE id = '$appt' ");
		if($sql){
			$message = "appointment Updated";
		}
 	}
	?>
	<div class="panel-heading">Edit Appointment</div>
	<div class="panel-body">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>	
			<form role="form" method="post" action="">
			<div class="form-group">
				<label for="email">Date:</label>
				<input type="date" class="form-control" id="adate" name="adate" value="<?=$row["adate"];?>">
			</div>
			<div class="form-group">
				<label for="email">Time:</label>
				<input type="text" class="form-control" id="atime" name="atime" value="<?=$row["atime"];?>">
			</div>
			<div class="form-group">
				<label for="email">Complaint:</label>
				<input type="text" class="form-control" id="comment" name="comment" value="<?=$row["comment"];?>">
			</div>
			<div class="form-group">
				<label for="email">Status:</label>
				<input type="text" class="form-control" id="status" value="<?=$row["status"];?>" name="status">
			</div>
			
			<input type="submit" name="updateAppointment" class="btn btn-success" value="Update" style="float:left;"> 
		</form>
	</div>
	<?php } ?>
</div> 			