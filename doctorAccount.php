<?php
if(isset($_POST["submit"])){
		
	$name  = $_POST["name"];
	$email = $_POST["email"];
	$tel   = $_POST["tel"];
	$sex   = $_POST["sex"];
	$id    = $_SESSION["id"];
	$dept  = $_POST["department"];
	$spec  = $_POST["speciality"];
	
		$sql = Query("UPDATE doctor SET name='$name', email='$email', department='$dept',speciality='$spec', tel='$tel', sex='$sex' WHERE docid='$id'");
		if($sql){  
			$message = "Account Updated";
		}else{
			$message = "Failed to update account";
		}
}

	$id = $_SESSION["id"];	
	$sql = Query("SELECT * FROM doctor WHERE docid='$id'");
	$row = fetchAssoc($sql);
?>
<div class="panel panel-primary">
	<div class="panel-heading">My Account</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?=$message;?></strong>
		</div>
		<?php } ?>
		<form role="form" method="post" action="" id="form" novalidate="novalidate">
			<div class="form-group">
				<label for="email">Name:</label>
				<input type="text" class="form-control" id="name" name="name" value="<?=$row["name"];?>" placeholder="Enter First Name">
			</div>
			<div class="form-group">
				<label for="email">Speciality:</label>
				<input type="text" class="form-control" id="speciality" name="speciality" value="<?=$row["speciality"];?>">
			</div>
			<div class="form-group">
				<label for="department">Department:</label>
				<select class="form-control" name="department" id="department">
				<option selected="selected"><?php
				$id =$row["department"];
						$sql2 = Query("SELECT * FROM departments WHERE dept_id='$id'");
						$doc = fetchAssoc($sql2);
						echo $doc["dept_name"];
				;?></option>
						
						<?php
						$sql1 = Query("SELECT * FROM departments");
						while($doc = fetchAssoc($sql1)){
						?>
						<option value="<?=$doc["dept_id"]; ?>"><?=$doc["dept_name"]; ?></option>
						<?php }?>
				</select>
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" class="form-control" id="email" value="<?=$row["email"];?>" name="email" placeholder="Enter email">
			</div>
			<div class="form-group">
				<label for="Sex">Sex:</label>
				<select class="form-control" name="sex" id="sex">
				<option selected="selected" value="Male">Male</option>
				<option value="Female">Female</option>
				</select>
			</div>
			<div class="form-group">
				<label for="tel">Contact Number:</label>
				<input type="text" class="form-control" value="<?=$row["tel"];?>" id="tel" name="tel" placeholder="Enter Contact">
			</div>
			<input type="submit" name="submit" class="btn btn-success" value="Update" style="float:left;"> 
		</form>
	</div>
</div> 

<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                name : {
                   required  : true,
                   minlength : 3
                },
                speciality : {
                    required : true,
                    minlength : 3
                },
                email : {
                    required : true,
                    email    : true
                },
                department : {
                	required :true
                },
                sex : {
                    required : true
                },
                tel : {
                    required : true,
                    digits   : true,
                    minlength : 11,
                    maxlength : 11
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>

