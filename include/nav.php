<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      
 <?php 
if (isset($_SESSION["user_type"])) {
    if ($_SESSION["user_type"] == "patient") {
        ?>
      <a class="navbar-brand" href="<?=WEB_ROOT; ?>view.php?mod=patient&view=Home"><strong>Hospital Appointment Booking System</strong></a>
      </div>
          <ul class="nav navbar-nav " >
              <li><a href="<?=WEB_ROOT; ?>view.php?mod=patient&view=Home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
              <li><a href="<?=WEB_ROOT; ?>view.php?mod=patient&view=BookAppointment"><span class="glyphicon glyphicon-time"></span> Book Appointment</a></li>
            
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li><a href="#"  class="btn btn-primary" style="color:#e6e6e6; background-color:#336699;" data-toggle="modal" data-target="#myModal">Notifications
            <?php
            $sql = Query("SELECT * FROM appointment WHERE patid = ".$_SESSION["patid"]."");
        $number = "";
        while ($row = fetchAssoc($sql)) {
            $date1 = strtotime(date("d-m-Y", strtotime($row["adate"])));
            $date2 = strtotime(date("d-m-Y"));
            $result= ($date1 - $date2)/86400;
        
            if (($result <= 2) && ($result >=0)) {
                $number++;
            }
        } ?>
            <span class="badge"> 
              <?php
              if ($number != 0) {
                  echo $number;
              } ?>
                  </span>
            </a>
            </li>
            <li><a href="<?=WEB_ROOT; ?>process.php?logOut"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
  <?php
    } elseif ($_SESSION["user_type"] == "doctor") {
        ?>

      <a class="navbar-brand" href="<?=WEB_ROOT; ?>view.php?mod=doctor&view=Home"><strong>Hospital Appointment Booking System</strong></a>
      </div>
          <ul class="nav navbar-nav " >
              <li><a href="<?=WEB_ROOT; ?>view.php?mod=doctor&view=Home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?=WEB_ROOT; ?>process.php?logOut"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>


  <?php
    } elseif ($_SESSION["user_type"] == "admin") {
        ?>

  <a class="navbar-brand" href="<?=WEB_ROOT; ?>view.php?mod=admin&view=Home"><strong>Hospital Appointment Booking System</strong></a>
      </div>
          <ul class="nav navbar-nav " >
              <li><a href="<?=WEB_ROOT; ?>view.php?mod=admin&view=Home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?=WEB_ROOT; ?>process.php?logOut"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
      
         
  <?php
    }
} else {
    ?>
   <a class="navbar-brand" href="#">Hospital Appointment Booking System</a>
    </div>
      <ul class="nav navbar-nav " >
        <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <li><a href="<?=WEB_ROOT; ?>view.php?mod=patient&view=bookAppointment"><span class="glyphicon glyphicon-time"></span> Book Appointment</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="register.php"><span class="glyphicon glyphicon-user"></span> Register</a></li>
        <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        
      </ul>
<?php
}
?>
  </div>
</nav>

<div class="container-fluid" style="margin:60px 5px 20px 0;">
