
<?php
require_once 'header.php';
require_once 'nav.php';

?>
<div class="row">
    
    <div class="col-sm-3">
      <?php require 'menu.php'; ?>
    </div>
    <div class="col-sm-9">
      <?php require $content;  ?>
    </div>

</div>

<?php

require_once 'footer.php'; 
?>
