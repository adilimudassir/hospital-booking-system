<div class="panel panel-primary">
    <?php if($_SESSION["user_type"] == "patient"){  ?>
    
     <div class="panel-heading">Patient Account</div>
      <div class="panel-body">
        <p><strong>Patient ID : <?=$_SESSION["patid"]; ?><br />
                   Name : <?=$_SESSION["name"]; ?></strong></p>
        <div class="list-group">
            <a href="<?=WEB_ROOT;?>view.php?mod=patient&view=Account" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> My Account</a>
            <a href="<?=WEB_ROOT;?>view.php?mod=patient&view=ChangePassword" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> Change Password</a>
            <a href="<?=WEB_ROOT;?>view.php?mod=patient&view=ViewAppointments" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> View Appointments</a>
            <a href="<?=WEB_ROOT;?>view.php?mod=patient&view=Treatments" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> Treatment History</a>
            <?php
            $newMsg = Query("SELECT COUNT(*) AS newMsg FROM pm WHERE(recipient = ".$_SESSION["patid"]." AND hasRead = 'no') ");
            $rows = fetchArray($newMsg);
            $msg = $rows["newMsg"];
            ?>
            <div class="w3-accordion">
                <a href="#" class="list-group-item" onclick="myAccFunc()" >
                    <span class="glyphicon glyphicon-triangle-right"></span>
                    <?php 
                        if($msg > 0){
                    ?>
                    <span class="badge"><?=$msg;?></span>
                    <?php } ?>
                    Messages&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"   ></span>
                </a>
        

                <div id="demoAcc" class="w3-accordion-content w3-white w3-card-4">
                    <a href="<?=WEB_ROOT;?>view.php?mod=patient&view=Inbox"><span class="glyphicon glyphicon-envelope"></span> Inbox 
                        <?php 
                            if($msg > 0){
                        ?>

                        <span class="badge"><?=$msg;?></span>
                        <?php 
                            } 
                        ?>
                    </a>
                    <a href="<?=WEB_ROOT;?>view.php?mod=patient&view=Compose"><span class="glyphicon glyphicon-pencil"></span> New Message</a>
                </div>

            </div>
        </div>
    <?php
      }else if($_SESSION["user_type"] == "doctor"){
      
      ?>
      <div class="panel-heading">Doctor Account</div>
      <div class="panel-body">
       <p><strong>
            Doctor ID : <?=$_SESSION["id"];?><br />
            Name : <?=$_SESSION["name"]; ?>
        </strong></p>
    

        <div class="list-group">
            <a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Account" class="list-group-item">
                <span class="glyphicon glyphicon-triangle-right"></span> My Account
            </a>
            <a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=ChangePassword" class="list-group-item">
                <span class="glyphicon glyphicon-triangle-right"></span> Change Password
            </a>
            <a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Appointments" class="list-group-item">
                <span class="glyphicon glyphicon-triangle-right"></span> View Appointments
            </a>
            <a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=AddTreatment" class="list-group-item">
                <span class="glyphicon glyphicon-triangle-right"></span> Add Treatment
            </a>
            <a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Treatments" class="list-group-item">
              <span class="glyphicon glyphicon-triangle-right"></span> My Treatments
            </a>
            <a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=ManageTimings" class="list-group-item">
              <span class="glyphicon glyphicon-triangle-right"></span> Manage Timings
            </a>

            
            <?php
                $newMsg = Query("SELECT COUNT(*) AS newMsg FROM pm WHERE(recipient = ".$_SESSION["id"]." AND hasRead = 'no') ");
            
                $rows = fetchArray($newMsg);
                $msg = $rows["newMsg"];
            ?>

        <div class="w3-accordion">
            <a href="#" class="list-group-item" onclick="myAccFunc()" >
            <span class="glyphicon glyphicon-triangle-right"></span>
            <?php 
                if($msg > 0){
            ?>

            <span class="badge"><?= $msg; ?></span>
            <?php 
                } 
            ?>

            Messages&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-triangle-bottom"></span>
            </a>
        

        <div id="demoAcc" class="w3-accordion-content w3-white w3-card-4">
            <a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Inbox"><span class="glyphicon glyphicon-envelope"></span> Inbox 
            <?php 
                if($msg > 0){
            ?>

            <span class="badge"><?=$msg;?></span>
            <?php } ?></a>
            <a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Compose"><span class="glyphicon glyphicon-pencil"></span> New Message</a>
        </div>

    </div>
    </div>
      <?php
      }else if($_SESSION["user_type"] == "admin"){
      
      ?>
      <div class="panel-heading">Admin Panel</div>
      <div class="panel-body">

 
    <div class="list-group">
    <a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Doctors" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> Doctors</a>
    <a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Departments" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> Departments</a>
    <a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Patients" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> Patients</a>
    <a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Appointments" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> Appointments</a>
    <a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Treatments" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> Treatments</a>
    <a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Admins" class="list-group-item"><span class="glyphicon glyphicon-triangle-right"></span> Admins</a>
    </div>

    <div class="list-group">
    <a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Profile" class="list-group-item"><span class="glyphicon glyphicon-user"></span>  My Profile</a>
            
    </div>
      
      <?php }?>


<script>
function myAccFunc() {
    var x = document.getElementById("demoAcc");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-grey";
    } else { 
        x.className = x.className.replace(" w3-show", "");
        x.previousElementSibling.className = 
        x.previousElementSibling.className.replace(" w3-green", "");
    }
}
</script>
</div>
</div>
