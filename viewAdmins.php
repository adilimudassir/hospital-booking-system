<?php
if(isset($_GET["del"])){
	if(Query("DELETE FROM admin WHERE id = ".$_GET["del"]."")){
		$message = "Admin deleted";
	}
}
?>
<div class="panel panel-primary">
	<?php if(!isset($_GET["edit"])){ ?>

	<div class="panel-heading">Manage Admins</div>
	<div class="panel-body">
	<div class="row" style="margin:5px;">
		<div class="col-sm-9">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
		</div>
		<div class="col-sm-3">
			<a class="btn btn-primary pull-right" href="<?=WEB_ROOT;?>view.php?mod=admin&view=addAdmin">Add new Admin</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No.</th>
					<th>Name</th>
					<th>Email</th>
					<th>Options</th>
				</tr>
			</thead>

			<tbody>
		<?php 
		$sql = Query("SELECT * FROM  admin");
		$n = 0;
		while($row1 = fetchAssoc($sql)){ ?>
					<tr>
						<td><?=++$n; ?></td>
						<td><?=$row1["name"]; ?></td>
						<td><?=$row1["email"]; ?></td>
						<td> <div align="center">
						
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Admins&del=<?=$row1["id"]; ?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
									</li>
								</ul>
							</div>
						

				</div></td>
						
					</tr>
	<?php } ?>
				</tbody>
			</table>
	</div>
</div>
	<?php } else { ?>
		
	<?php } ?>
</div> 			