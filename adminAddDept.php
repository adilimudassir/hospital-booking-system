<?php
if(isset($_POST["submit"])){
    $name = $_POST["name"];
   if(!preg_match("/^[a-zA-Z ]*$/",$name)){
        $message = "Name can only contain characters";
    } else {
        $sql = Query("INSERT INTO `departments`(`dept_id`, `dept_name`) VALUES (NULL,'$name')");
        if($sql){
            $message = "Added. The Department is: ".$name;
        } else {
            "Operation Failed";
        }
    
}
}
?>

<div class="panel panel-primary">
    <div class="panel-heading">Add New Department</div>

    <div class="panel-body">
    <?php if(isset($message) and $message != ""){ ?>
        <div class="alert alert-info">
             <strong><?=$message; ?></strong>
        </div>
    <?php }  ?>
        <form class="form-horizontal" method="POST" action="" id="form" novalidate="novalidate">
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Name</label>
                <div class="col-md-10">
                    <input id="name" type="text" class="form-control" name="name" required autofocus />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary" />
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                name : {
                   required  : true,
                   minlength : 3
                },
                speciality : {
                    required : true,
                    minlength : 3
                },
                department : {
                    required : true
                },
                email : {
                    required : true,
                    email    : true
                },
                sex : {
                    required : true
                },
                tel : {
                    required : true,
                    digits   : true,
                    minlength : 11,
                    maxlength : 11
                },
                password : {
                    required : true,
                    minlength : 6
                },
                cpassword : {
                    required : true,
                    equalTo  : '#password'
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>

