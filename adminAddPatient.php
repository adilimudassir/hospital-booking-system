<?php
if(isset($_POST["submit"])){
    $sql = Query("SELECT MAX(patid) FROM patient");
    $row = fetchArray($sql);
    
    $id = $row[0];
    $id++;
        
    $fname = $_POST["fname"];
    $lname = $_POST["lname"];
    $password = $_POST["password"];
    $cpassword = $_POST["cpassword"];
    $email = $_POST["email"];
    $tel = $_POST["tel"];
    $sex = $_POST["sex"];
    
    if(!preg_match("/^[a-zA-Z ]*$/",$fname)){
        $message = "First name can only contain characters";
    } else if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
        $message = "Last Name name can only contain characters";
    } else {
        $sql = Query("INSERT INTO patient (
                                                patid,
                                                fname,
                                                lname,
                                                pwd,
                                                email,
                                                tel,
                                                sex
                                                )
                                        VALUES (
                                                '$id',
                                                '$fname',
                                                '$lname',
                                                '$password',
                                                '$email',
                                                '$tel',
                                                '$sex'
                                                )");

        if($sql){
            $message = "Added. The Patient ID is: ".$id;
        } else {
            $message = "Operation Failed";
        }
    
}
}
?>

<div class="panel panel-primary">
    <div class="panel-heading">Add New Patient</div>

    <div class="panel-body">
    <?php if(isset($message) and $message != ""){ ?>
        <div class="alert alert-info">
             <strong><?=$message; ?></strong>
        </div>
    <?php }  ?>
        <form class="form-horizontal" method="POST" action="" id="form" novalidate="novalidate">
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">First Name</label>

                <div class="col-md-10">
                    <input id="fname" type="text" class="form-control" name="fname" required autofocus />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Last Name</label>
                <div class="col-md-10">
                    <input id="lname" type="text" class="form-control" name="lname" required/>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-md-2 control-label">E-Mail Address</label>
                <div class="col-md-10">
                    <input id="email" type="email" class="form-control" name="email" value="" required />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Sex</label>
                <div class="col-md-10">
                   <select required class="form-control" name="sex" id="sex">
                        <option value="">---SELECT----</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Phone Number</label>
                <div class="col-md-10">
                    <input id="tel" type="text" class="form-control" name="tel" required/>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-md-2 control-label">Password</label>
                <div class="col-md-10">
                    <input id="password" type="password" class="form-control" name="password" required>
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-2 control-label">Confirm Password</label>

                <div class="col-md-10">
                    <input id="password-confirm" type="password" class="form-control" name="cpassword" required>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <input type="submit" name="submit" value="Add new" class="btn btn-primary" />
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                fname : {
                   required  : true,
                   minlength : 3
                },
                lname : {
                    required : true,
                    minlength : 3
                },
                email : {
                    required : true,
                    email    : true
                },
                sex : {
                    required : true
                },
                tel : {
                    required : true,
                    digits   : true,
                    minlength : 11,
                    maxlength : 11
                },
                password : {
                    required : true,
                    minlength : 6
                },
                cpassword : {
                    required : true,
                    equalTo  : '#password'
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>

