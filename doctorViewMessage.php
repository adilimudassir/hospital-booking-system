<?php
	$msgID = $_GET["id"];
	$sql = Query("SELECT * FROM pm WHERE id = ".$msgID."");
	$msgRow = fetchAssoc($sql);
	$recipient = $msgRow["recipient"];
	
	$sql = Query("UPDATE pm SET hasRead = 'yes' WHERE id = ".$msgID." AND recipient = ".$_SESSION["id"]."");
	
	$subject = $msgRow["subject"];
	$sender = $msgRow["sender"];
	$id = $msgRow["id"];
	$message = $msgRow["message"];
?>
<div class="panel panel-primary">
	<div class="panel-heading">Reading Message</div>
	<div class="panel-body">
		
			<h2 style="color:#337ab7; font-weight:bold;"><?php echo $subject; ?></h2>
			<hr>
			<div class="row">
				<div class="col-sm-1" style="color:#666; text-align:right; ">from:</div>
				<div class="col-sm-4" style="background-color:#FFF;"><?php
						
							$query = Query("SELECT * FROM patient WHERE patid =".$sender."");
							$info = fetchAssoc($query);
							echo $info["fname"]." ". $info["lname"];
							?></div><br>
				<div class="col-sm-1" style="color:#666; text-align:right;">to:</div>
				<div class="col-sm-4" style=""><?php echo $_SESSION["name"]; ?></div><br />
				<div class="col-sm-1" style="color:#666; text-align:right;">date:</div>
				<div class="col-sm-4" style=" "><?php 
				
				$date = $msgRow["timestamp"];
				$day = date("D", strtotime($date));
				$mon = date("M d", strtotime($date));
				$yr = date("Y", strtotime($date));
				$time = date("h:ia", strtotime($date));
				echo $day.", ".$mon.", ".$yr." at ".$time;
				?></div><br>
				<div class="col-sm-1" style="color:#666;text-align:center; padding-left:0px">subject:</div>
				<div class="col-sm-4" style=""><?php echo $subject; ?></div><br />
			</div>  
			<hr height="100px">
			<p class="message"><?php echo $message; ?></p>	
	</div>
</div> 			