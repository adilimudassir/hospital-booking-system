<?php
if(isset($_POST["submit"])){
	
	if($_POST["name"] != ""){	
		$name = $_POST["name"];
		$password = $_POST["password"];
		$cpassword = $_POST["cpassword"];
		$email = $_POST["email"];
		
		 if(!preg_match("/^[a-zA-Z ]*$/",$fname)){
       		 $message = "Name can only contain characters";
   		 } else {
			$sql = Query("INSERT INTO admin (
													name,
													password,
													email
													)
											VALUES (
													'$name',
													'$password',
													'$email'
													)");
		}

		if($sql){
			$message = "Admin Created";
		}
	}

}

?>
<div class="panel panel-primary">
	<div class="panel-heading">New Admin</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
		<form role="form" method="post" action="" id="form" novalidate="novalidate">
					<div class="form-group">
						<label for="email">Name:</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="Enter First Name">
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="password">Password:</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
					</div>
					<div class="form-group">
						<label for="password">Confirm Password:</label>
						<input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm password">
					</div>
					<input type="submit" name="submit" class="btn btn-primary" value="Submit" style="float:left;"> 
					
					
				</form>
	</div>
</div> 			

<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                name : {
                   required  : true,
                   minlength : 3
                },
                email : {
                    required : true,
                    email    : true
                },
                password : {
                    required : true,
                    minlength : 6
                },
                cpassword : {
                    required : true,
                    equalTo  : '#password'
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>

