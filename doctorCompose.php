<?php
if(isset($_POST["send"])){
	
	$recipient = $_POST["recipient"];
	$sender    = $_SESSION["id"];
	$subject   = $_POST["subject"];
	$message   = $_POST["message"];

	if(isset($subject) && $subject != ""){
		$sql = Query("INSERT INTO pm (
									recipient,
									sender,
									subject,
									message,
									hasRead
									)
							VALUES (
									'$recipient',
									'$sender',
									'$subject',
									'$message',
									'no'
									)"
							);
		if($sql){
			$message = "Message sent!";
			
		}else{
			$message = "Message not sent!";
		}
} else{
		$message = "Please enter subject!";
	}
}
?>		
<div class="panel panel-primary">
	<div class="panel-heading">New Message</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?=$message;?></strong>
		</div>
		<?php } ?>
		<form role="form" method="post" action="" id="form" novalidate="novalidate">
					<div class="form-group">
						<label for="patient ID">Recipient:</label>
							<?php
							if(isset($_GET["id"])){ ?>
								<select required class="form-control" name="recipient" id="recipient">
								
								<?php
								$sql = Query("SELECT * FROM patient WHERE patid =".$_GET["id"]."");
								$doc = fetchAssoc($sql);
								?>
								<option value="<?=$doc["patid"]; ?>" selected="selected" readonly="readonly" ><?=$doc["fname"]." ".$doc["lname"]; ?></option>
								
								<?php
									
								}else { ?>
								<select required class="form-control" name="recipient" id="recipient">
								<option value="">---SELECT---</option>
								<?php
									$sql = Query("SELECT * FROM patient");
									while($doc = fetchAssoc($sql)){
									?><option value="<?=$doc["patid"]; ?>"><?=$doc["fname"]." ".$doc["lname"]; ?></option>
									
								<?php }} ?>
	
							</select>
					</div>
					<div class="form-group">
						<label for="email">Subject:</label>
						<input required type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject">
					</div>
					<div class="form-group">
						<label for="comment">Message:</label>
						<textarea required="required" class="form-control" rows="7" id="message" name="message"></textarea>
					</div>
					<input type="submit" name="send" class="btn btn-primary" value="Send" style="float:left;"> 
					
					
				</form>
	</div>
</div> 			

<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                recipient : {
                   required  : true
                },
                subject : {
                    required : true
                },
                message : {
                    required : true
                }
             },
            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>