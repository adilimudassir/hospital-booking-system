<?php
require_once './library/config.php';
require_once './library/functions.php';

//$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$mod = (isset($_GET['mod']) && $_GET['mod'] != '') ? $_GET['mod'] : '';
$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

if($mod == 'patient'){
	switch ($view) {
		case 'Home' :
			$content 	= 'patient.php';		
			$pageTitle 	= 'Patient Home';
			break;

		case 'BookAppointment' :
			$content 	= 'patient.php';		
			$pageTitle 	= 'Patient Home';
			break;

		case 'AppointmentTime' :
			$content 	= 'appointmenttime.php';		
			$pageTitle 	= 'Appointment Time';
				break;

		case 'MakeAppointment' :
			$content 	= 'makeAppointment.php';		
			$pageTitle 	= 'Make Appointment';
			break;

		case 'ViewAppointments' :
			$content    = 'viewAppointments.php';
			$pageTitle  = 'My Appointments';
			break;

		case 'Account' :
			$content   = 'patientAccount.php';
			$pageTitle =  'My Account';
			break;

		case 'ChangePassword' :
			$content   = 'patientChangePass.php';
			$pageTitle =  'Change Password';
			break;

		case 'Treatments' :
			$content   = 'patientTreatment.php';
			$pageTitle =  'Treatments';
			break;

		case 'Compose' :
			$content   = 'patientCompose.php';
			$pageTitle =  'New Message';
			break;

		case 'Inbox' :
			$content   = 'patientInbox.php';
			$pageTitle =  'Inbox';
			break;

		case 'patientViewMessage' :
			$content   = 'patientViewMessage.php';
			$pageTitle =  'Read Message';
			break;
	
	}//switch
}
elseif ($mod == 'doctor'){
	switch ($view) {
		case 'Home' :
			$content 	= 'doctorAppointment.php';		
			$pageTitle 	= 'Home';
			break;

		case 'Account' :
			$content 	= 'doctorAccount.php';		
			$pageTitle 	= 'My Account';
			break;

		case 'ChangePassword' :
			$content 	= 'doctorChangePass.php';		
			$pageTitle 	= 'Change Password';
			break;

		case 'Appointments' :
			$content 	= 'doctorAppointment.php';		
			$pageTitle 	= 'My Appointment';
			break;

		case 'AddTreatment' :
			$content 	= 'addTreatment.php';		
			$pageTitle 	= 'Add new treatment';
			break;

		case 'Treatments' :
			$content 	= 'treatmentHistory.php';		
			$pageTitle 	= 'Treatments History';
			break;

		case 'ManageTimings' :
			$content 	= 'manageTimings.php';		
			$pageTitle 	= 'Manage Timings';
			break;

		case 'Inbox' :
			$content 	= 'doctorInbox.php';		
			$pageTitle 	= 'Inbox';
			break;

		case 'doctorViewMessage' :
			$content   = 'doctorViewMessage.php';
			$pageTitle =  'Read Message';
			break;

		case 'Compose' :
			$content   = 'doctorCompose.php';
			$pageTitle =  'New Message';
			break;
	
	}//switch
}
elseif ($mod == 'admin') {
	switch ($view) {
	
		case 'Home' :
			$content 	= 'admin.php';		
			$pageTitle 	= 'Home';
		    break;

		    case 'Departments' :
			$content 	= 'adminDepartments.php';		
			$pageTitle 	= 'Departments';
		    break;

		case 'Patients' :
			$content 	= 'adminPatients.php';		
			$pageTitle 	= 'Manage Patients';
		    break;

		case 'AddPatient' :
			$content 	= 'adminAddPatient.php';		
			$pageTitle 	= 'Add Patient';
		    break;
	   case 'AddDept' :
			$content 	= 'adminAddDept.php';		
			$pageTitle 	= 'Add Department';
		    break;


		case 'Doctors' :
			$content 	= 'adminDoctors.php';		
			$pageTitle 	= 'Manage Doctors';
		    break;

		case 'AddDoctor' :
			$content 	= 'adminAddDoctor.php';		
			$pageTitle 	= 'Add Doctors';
		    break;

		case 'Appointments' :
			$content 	= 'adminAppointments.php';		
			$pageTitle 	= 'Manage Appointments';
		    break;

		case 'Treatments' :
			$content 	= 'adminTreatments.php';		
			$pageTitle 	= 'Manage Treatments';
		    break;

		case 'Admins' :
			$content 	= 'viewAdmins.php';		
			$pageTitle 	= 'Manage Admins';
		    break;

		case 'addAdmin' :
			$content 	= 'addAdmin.php';		
			$pageTitle 	= 'New Admin';
		    break;

		case 'Profile' :
			$content 	= 'adminProfile.php';		
			$pageTitle 	= 'My Profile';
		    break;
	
	}//switch

}//if

require_once './include/template.php';

?>
