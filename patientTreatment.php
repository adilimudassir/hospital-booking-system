<div class="panel panel-primary">
	<div class="panel-heading">My Treatments</div>
	<div class="panel-body">
		
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No.</th>
					<th>Appointment <br />Date time</th>
					<th>Date</th>
					<th>Diagnoses</th>
					<th>Prescriptions</th>
					<th>Dosage</th>
					<th>Doctor Name</th>
					<th>Note</th>
					<th>Options</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			$sql1 = Query("SELECT * FROM treatment WHERE patid =".$_SESSION["patid"]."");
			$n = 0;
			while($row1 = fetchAssoc($sql1)){
			$n++; ?>
				<tr>
					<td><?=$n;?></td>
					<td>
					<?php
					$sql = Query("SELECT * FROM appointment WHERE id =".$row1["appt_id"]."");
					$appoin = fetchAssoc($sql);
					echo $appoin["atime"].", ".$appoin["adate"]; ?></td>
					<td><?=date("d-m-Y", strtotime($row1["date"])); ?></td>
					<td><?=$row1["disease"]; ?></td>
					<td><?=$row1["treatment"];?></td>
					<td><?=$row1["dosage"]; ?></td>
					<td><?php
					
					$sql = Query("SELECT * FROM doctor WHERE docid =".$row1["docid"]."");
					$doc = fetchAssoc($sql);


					echo $doc["name"]; ?></td>
					<td><?=$row1["note"]; ?></td>
					<td> 
						<div align="center">
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=patient&view=Compose&id=<?=$row1["docid"]; ?>"><span class="glyphicon glyphicon-envelope"></span> Message Doctor</a>
									</li>
								
								</ul>
							</div>
						</div>
					</td>
					
				</tr>
			<?php } ?>
				</tbody>
			</table>
	</div>
</div> 			