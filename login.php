<?php
require_once './library/config.php';
require_once './library/functions.php';


if (isset($_POST['submit'])) {
    $id = $_POST["id"];
    $password = $_POST["password"]; 

	$result = Login($id,$password);
	
	if ($result != '') {
		$message = $result;
	}
}

 include 'include/header.php'; 
      include 'include/nav.php';
?>


  <div class="row" style ="margin-top: 100px;">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                     <?php if(isset($message)){ ?>
                        <div class="alert alert-info">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><?=$message;?></strong>
                        </div>
                    <?php } ?>
                    <form class="form-horizontal" method="POST" action="" id="Login-form" novalidate="novalidate">
                    
                        <div class="form-group">
                            <label for="ID" class="col-md-4 control-label">ID</label>
                            <div class="col-md-6">
                                <input id="id" type="text" class="form-control" name="id" autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                        
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                               <input type="submit" name="submit" value="Login" class="btn btn-primary" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include('include/footer.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#Login-form").validate({
            rules: {
                id : {
                  required : true
                },
                password : {
                  required :true
                }
            },
            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>
