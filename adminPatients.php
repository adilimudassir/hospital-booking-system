<?php
if(isset($_GET["del"])){
	if(Query("DELETE FROM patient WHERE patid = ".$_GET["del"]."")){
		$message = "patient deleted";
	}
}
?>
<div class="panel panel-primary">
	<?php if(!isset($_GET["edit"])){ ?>

	<div class="panel-heading">Manage Patients</div>
	<div class="panel-body">
	<div class="row" style="margin:5px;">
		<div class="col-sm-9">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
		</div>
		<div class="col-sm-3">
			<a class="btn btn-primary pull-right" href="<?=WEB_ROOT;?>view.php?mod=admin&view=AddPatient">Add new Patient</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No.</th>
					<th>ID</th>
					<th>Name</th>
					<th>Sex</th>
					<th>Email</th>
					<th>Phone Number</th>
					<th>Registration date</th>
					<th>Options</th>
				</tr>
			</thead>

			<tbody>
		<?php 
		$sql = Query("SELECT * FROM  patient");
		$n = 0;
		while($row1 = fetchAssoc($sql)){ ?>
					<tr>
						<td><?=++$n; ?></td>
						<td><?=$row1["patid"]; ?></td>
						<td><?=$row1["fname"]." ".$row1["lname"]; ?></td>
						<td><?=$row1["sex"]; ?></td>
						<td><?=$row1["email"]; ?></td>
						<td><?=$row1["tel"]; ?></td>
						<td><?=date("d-m-Y", strtotime($row1["date"])); ?></td>
						<td> <div align="center">
						
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Patients&del=<?=$row1["patid"]; ?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
									</li>
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Patients&edit=<?=$row1["patid"]; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
									</li>
								</ul>
							</div>
						

				</div></td>
						
					</tr>
	<?php } ?>
				</tbody>
			</table>
	</div>
</div>
	<?php } else { 
		$id = $_GET["edit"];

		if(isset($_POST["updatePatient"])){
			$fname = $_POST["fname"];
		    $lname = $_POST["lname"];
		    $pwd   = $_POST["pwd"];
		    $cpwd  = $_POST["cpwd"];
		    $email = $_POST["email"];
		    $tel   = $_POST["tel"];
		    $sex   = $_POST["sex"];
			
			$sql = Query("UPDATE patient  set   fname = '$fname',
			             						lname = '$lname',
			             						email = '$email',
											    tel   = '$tel',
											    sex   = '$sex'
 										  
										  WHERE patid = '$id'
											");
			if($sql){
				$message = "Patient updated";
			}else{
				$message = "Operation failed";
			}
		}

		
		$sql = Query("SELECT * FROM patient WHERE patid ='$id'");
		$row = fetchAssoc($sql);

		?>

		<div class="panel-heading">Edit Patient</div>
		<div class="panel panel-body">
			<?php if(isset($message) and $message != ""){ ?>
		        <div class="alert alert-info">
		             <strong><?=$message; ?></strong>
		        </div>
		    <?php }  ?>
        <form class="form-horizontal" method="POST" action="">
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">First Name</label>

                <div class="col-md-10">
                    <input id="fname" type="text" value="<?=$row["fname"];?>" class="form-control" name="fname" required autofocus />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Last Name</label>
                <div class="col-md-10">
                    <input id="lname" type="text" value="<?=$row["lname"];?>" class="form-control" name="lname" required/>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-md-2 control-label">E-Mail Address</label>
                <div class="col-md-10">
                    <input id="email" type="email" value="<?=$row["email"];?>" class="form-control" name="email" value="" required />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Sex</label>
                <div class="col-md-10">
                   <select required class="form-control" value="<?=$row["sex"];?>" name="sex" id="sex">
                        <option selected="selected" value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="name"  class="col-md-2 control-label">Phone Number</label>
                <div class="col-md-10">
                    <input id="tel" type="text" value="<?=$row["tel"];?>" class="form-control" name="tel" required/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <input type="submit" name="updatePatient" value="Update" class="btn btn-success" />
                </div>
            </div>
        </form>
		</div>
	<?php } ?>
</div> 			