<?php
require_once './library/config.php';
require_once './library/functions.php';

if(isset($_POST["submit"])){
    $sql = Query("SELECT MAX(patid) FROM patient");
    $row = fetchArray($sql);
    
    $id = $row[0];
    $id++;
        
    $fname = $_POST["fname"];
    $lname = $_POST["lname"];
    $pwd = $_POST["pwd"];
    $cpwd = $_POST["cpwd"];
    $email = $_POST["email"];
    $tel = $_POST["tel"];
    $sex = $_POST["sex"];

    
     
    if(!preg_match("/^[a-zA-Z ]*$/",$fname)){
        $message = "First name can only contain characters";
    } else if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
        $message = "Last Name name can only contain characters";
    } else {
        $sql = Query("INSERT INTO patient (
                                                patid,
                                                fname,
                                                lname,
                                                pwd,
                                                email,
                                                tel,
                                                sex
                                                )
                                        VALUES (
                                                '$id',
                                                '$fname',
                                                '$lname',
                                                '$pwd',
                                                '$email',
                                                '$tel',
                                                '$sex'
                                                )");

        if($sql){
            $message = "Your Registration is Successful. Your Patient ID is: ".$id; ?>
                <!-- <script
    src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
                <script>

                    $.get("http://smsclone.com/components/com_spc/smsapi.php?username=nabilabawa&password=1210310142&sender=UDUTH&recipient='<?=$tel;?>'&message='Your Accound has been created. Your Patient ID is <?=$id;?>'")
                </script> -->
          <?php  
        } else {
           $message =  "Operation Failed";
        }
    
}
}

include 'include/header.php'; 
      include 'include/nav.php';
?>

<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Patient Registration</div>

                <div class="panel-body">
                <?php if(isset($message) and $message != ""){ ?>
                    <div class="alert alert-info">
                         <strong><?=$message; ?></strong>
                    </div>
                <?php } else{ ?>
                    <form class="form-horizontal" method="POST" action="" id="form" novalidate="novalidate">
                        <div class="form-group">
                            <label for="name" class="col-md-2 control-label">First Name</label>

                            <div class="col-md-10">
                                <input id="fname" type="text" class="form-control" name="fname"  autofocus />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-2 control-label">Last Name</label>
                            <div class="col-md-10">
                                <input id="lname" type="text" class="form-control" name="lname" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-2 control-label">E-Mail Address</label>
                            <div class="col-md-10">
                                <input id="email" type="email" class="form-control" name="email" value=""  />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-2 control-label">Sex</label>
                            <div class="col-md-10">
                               <select  class="form-control" name="sex" id="sex">
                                    <option value="" selected="">Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-2 control-label">Phone Number</label>
                            <div class="col-md-10">
                                <input id="tel" type="text" class="form-control" name="tel" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-2 control-label">Password</label>
                            <div class="col-md-10">
                                <input id="password" type="password" class="form-control" name="pwd" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password_confirm" class="col-md-2 control-label">Confirm Password</label>

                            <div class="col-md-10">
                                <input id="cpwd" type="password" class="form-control" name="cpwd" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <input type="submit" name="submit" value="Register" class="btn btn-primary" />
                            </div>
                        </div>
                    </form>
                    <?php }  ?>
                </div>
            </div>

        </div>
    </div>
<?php include 'include/footer.php';?>

<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                fname : {
                   required  : true,
                   minlength : 3
                },
                lname : {
                    required : true,
                    minlength : 3
                },
                email : {
                    required : true,
                    email    : true
                },
                sex : {
                    required : true
                },
                tel : {
                    required : true,
                    digits   : true,
                    minlength : 11,
                    maxlength : 15
                },
                pwd : {
                    required : true,
                    minlength : 6
                },
                cpwd : {
                    required : true,
                    equalTo  : '#password'
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }


          });

      })
</script>

