<?php
if(isset($_GET["del"])){
	if(Query("DELETE FROM appointment WHERE id = ".$_GET["del"]."")){
		$message = "Appointment Deleted";
	}
}

if(isset($_GET["approve"])){
	if(Query("UPDATE appointment SET status='Approved' WHERE id = ".$_GET["approve"]."")){
		$message = "appointment Aprroved";
	}
}
?>
<div class="panel panel-primary">
	<?php if(!isset($_GET["edit"])){?>
		<div class="panel-heading">My Appointments</div>
		<div class="panel-body">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>App.No</th>
						<th>Date  Time</th>
						<th>Appointment<br />Date</th>
						<th>Appointment <br /> Time</th>
						<th>Patient Name</th>
						<th>Comment</th>
						<th>Status</th>
						<th>Options</th>
					</tr>
				</thead>

				<tbody>
				<?php 
				$sql = Query("SELECT * FROM  appointment WHERE docid =".$_SESSION['id']."");
				$n = 0;
				while($row1 = fetchAssoc($sql)){
				$n++; ?>
					<tr>
						<td><?=$n; ?></td>
						<td><?=date("d-m-Y h:i:s", strtotime($row1["date"])); ?></td>
						<td><?=date("d-m-Y", strtotime($row1["adate"])); ?></td>
						<td><?=$row1["atime"]; ?></td>
						<td><?php

						$result= Query("SELECT * FROM patient WHERE patid=".$row1["patid"]."");
						$row = mysqli_fetch_array($result);
							echo $row["fname"]." ".$row["lname"];
						?></td>
						<td><?=$row1["comment"]; ?></td>
						<td><?php
						echo "<span class=\"text-primary\">".$row1["status"]."</span>"; ?></td>
						<td> 
							<div align="center">
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li>
											<a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Appointments&edit=<?=$row1["id"]; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
										</li>
										<li>
											<a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Appointments&del=<?=$row1["id"]; ?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
										</li>
										<li>
											<a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Appointments&approve=<?=$row1["id"]; ?>""><span class="glyphicon glyphicon-ok"></span>Approve</a>
										</li>
										<li>
											<a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=Compose&id=<?=$row["patid"]; ?>"><span class="glyphicon glyphicon-envelope"></span> Message Patient</a>
										</li>
									</ul>
								</div>
							</div>
						</td>
					</tr>
		<?php } ?>
					</tbody>
				</table>
		</div>
	<?php } else { 

	$id = $_SESSION["id"];	
	$appt = $_GET["edit"];
	$sql = Query("SELECT * FROM  appointment WHERE docid = $id AND id = $appt");
	$row = fetchAssoc($sql);
	if(isset($_POST["updateAppointment"])){
		$date = $_POST["adate"];
		$time = $_POST["atime"];		
		$status = $_POST["status"];

		$sql = Query("UPDATE appointment SET adate = '$date',  atime = '$time', status = '$status' WHERE id = '$appt' ");
		if($sql){
			$message = "appointment Updated";
		}
 	}
	?>
	<div class="panel-heading">Update Appointment</div>
	<div class="panel-body">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>	
			<form role="form" method="post" action="" id="form" novalidate="novalidate">
			<div class="form-group">
				<label for="email">Date:</label>
				<input type="date" class="form-control" id="adate" name="adate" value="<?=$row["adate"];?>">
			</div>
			<div class="form-group">
				<label for="email">Time:</label>
				<input type="text" class="form-control" id="atime" name="atime" value="<?=$row["atime"];?>">
			</div>
			
			<div class="form-group">
				<label for="email">Status:</label>
				<input type="text" class="form-control" id="status" value="<?=$row["status"];?>" name="status">
			</div>
			
			<input type="submit" name="updateAppointment" class="btn btn-success" value="Update" style="float:left;"> 
		</form>
	</div>
	<?php } ?>

</div> 			
<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                adate : {
                   required  : true,
                   date : true
                },
                atime : {
                    required : true,
                    minlength : 5
                },
                status : {
                    required : true,
                    minlength    : 5
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>

