<?php 
$docid = $_SESSION["id"];
$sql = Query("SELECT * FROM timings WHERE docid ='$docid'");
$rec=numRows($sql);
if(!isset($_GET["update"]) and $rec==0){
	
	if (isset($_POST["submit"])){
		$day = $_POST['day'];
		$timing = $_POST['from'].",".$_POST['to'];
		$id = $_SESSION["id"];
		$query = Query("INSERT INTO `timings`(`docid`, `day`, `timings`) VALUES ($id,'$day','$timing')");
		if($query){
			$message = "Added Successfully";
		} else {
			$message = "Operation Failed";
		}
	}
	?>
<div class="panel panel-primary">
	<div class="panel-heading">Manage Timings</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
		<form method="post" action=""> 
			<div class="form-group">
				<label for="days"><h2 class="text-primary">Days:</h2></label>
				<select name="day" id="days" class="form-control">
					<?php $days = array('Monday','Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday','Sunday');
					for ($i=0; $i <=count($days) ; $i++) { 
						echo "<option>".$days[$i]."</option>";
					}

				 ?>
				</select>
			</div>
			<div class="form-group">
				<label for="comment"><h2 class="text-primary">Timings:</h2></label>
				<input type="time" name="from" class="form-control" placeholder="Free From"> - <input type="time" class="form-control" name="to" placeholder="Free To">
			</div>
			<input type="submit" class="btn btn-primary" name="submit" value="Submit" />
		</form>
	
		<?php
		}else if (isset($_GET["update"]) and isset($_GET['edit'])){
			$id = $_SESSION["id"];
			$query = Query("SELECT * FROM timings WHERE docid ='$id' AND timing_id ='$_GET[edit]'");
			$row = fetchAssoc($query);
			if (isset($_POST["submit"])){
				$timing = $_POST["from"].",".$_POST['to'];
				$query = Query("UPDATE timings set timings='$timing' WHERE docid='$id'");
				if($query){
					$message = "updated Successfully";
				} else {
					$message = "Operation failed";
				}
			}
			?>
		<div class="panel panel-primary">
		<div class="panel-heading">Update Timings</div>
		<div class="panel-body">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
			<form method="post" action=""> 
			<div class="form-group">
				<label for="days"><h2 class="text-primary">Days:</h2></label>
				<select name="day" id="days" class="form-control">
					<option selected><?php echo $row['day']?></option>
					<?php $days = array('Monday','Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday','Sunday');
					for ($i=0; $i <=count($days) ; $i++) { 
						echo "<option>".$days[$i]."</option>";
					}
					$times = explode(',', $row['timings']);
				 ?>
				</select>
			</div>
			<div class="form-group">
				<label for="comment"><h2 class="text-primary">Timings:</h2></label>
				<input type="time" name="from" value="<?php echo $times[0] ?>" class="form-control" placeholder="Free From"> - <input type="time" value="<?php echo $times[1] ?>" class="form-control" name="to" placeholder="Free To">
			</div>
			<input type="submit" class="btn btn-primary" name="submit" value="Submit" />
		</form>
		
		<?php
		}elseif (isset($_GET['add'])) {

		if (isset($_POST["submit"])){
		$day = $_POST['day'];
		$timing = $_POST['from'].",".$_POST['to'];
		$id = $_SESSION["id"];
		$query = Query("INSERT INTO `timings`(`docid`, `day`, `timings`) VALUES ($id,'$day','$timing')");
		if($query){
			$message = "Added Successfully";
		} else {
			$message = "Operation Failed";
		}
	}
			?>
		<div class="panel panel-primary">
		<div class="panel-heading">Add Timings</div>
		<div class="panel-body">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
			<form method="post" action=""> 
			<div class="form-group">
				<label for="days"><h2 class="text-primary">Days:</h2></label>
				<select name="day" id="days" class="form-control">
					<?php $days = array('Monday','Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday','Sunday');
					for ($i=0; $i <=count($days) ; $i++) { 
						echo "<option>".$days[$i]."</option>";
					}
					$times = explode(',', $row['timings']);
				 ?>
				</select>
			</div>
			<div class="form-group">
				<label for="comment"><h2 class="text-primary">Timings:</h2></label>
				<input type="time" name="from"  class="form-control" placeholder="Free From"> - <input type="time"  class="form-control" name="to" placeholder="Free To">
			</div>
			<input type="submit" class="btn btn-primary" name="submit" value="Submit" />
		</form>
			
		<?php } else{ ?>
		<div class="panel panel-primary">
	<div class="panel-heading">Manage Timings</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
			<a class="btn btn-primary" href="<?=WEB_ROOT;?>view.php?mod=doctor&view=ManageTimings&add=yes">Add Timings</a>
			<br>
			<br>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style="text-align:center;">Day</th>
						<th style="text-align:center;">Timings</th>
						<th style="text-align:center;"></th>
					</tr>
				</thead>
				<tbody><?php
				$sql = Query("SELECT * FROM timings WHERE docid = ".$_SESSION["id"]."");
				while($row=fetchAssoc($sql)){
				$timeRecord = $row["timings"];
				$timeSlots = explode(" ", $timeRecord);
	
	
	?>

					<tr>
					<td><? echo $row['day']?></td>
					<td>
						<?php
						$times = explode(',', $row['timings']);

						$from = $times[0];
						$to = $times[1];

						echo getTime($from)." to ".getTime($to);

						 ?>
							
						</td>
						<td>	<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
							<span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=ManageTimings&update=yes&id=<?php echo $row['timing_id'] ?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
									</li>
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=doctor&view=ManageTimings&update=yes&edit=<?php echo $row['timing_id'] ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
									</li>
							</ul>
						</div></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
		<?php
		}
		?>
		</div>
	</div>
</div> 			