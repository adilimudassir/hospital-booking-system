<?php
if(isset($_GET["del"])){
	if(Query("DELETE FROM treatment WHERE id = ".$_GET["del"]."")){
		$message = "Treatment Deleted";
	}
}
?>
<div class="panel panel-primary">
	<?php if(!isset($_GET["edit"])){ ?>
	<div class="panel-heading">Manage Treatments</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No.</th>
					<th>Appointment Date</th>
					<th>Appointment Time</th>
					<th>Diagnoses</th>
					<th>Prescriptions</th>
					<th>Dosage</th>
					<th>Doctor Name</th>
					<th>Patient Name</th>
					<th>Note</th>
					<th>Options</th>
				</tr>
			</thead>

						<tbody>
			<?php 
			$sql1 = Query("SELECT * FROM treatment");
			$n = 0;
			while($row1 = fetchAssoc($sql1)){ ?>
				<tr>
					<td><?=++$n; ?></td>
					<td>
					<?php
					$sql = Query("SELECT * FROM appointment WHERE id =".$row1["appt_id"]."");
					$appoin = fetchAssoc($sql);
					echo $appoin["adate"]; ?></td>
					<td><?=$appoin["atime"]; ?></td>
					<td><?=$row1["disease"]; ?></td>
					<td><?=$row1["treatment"];?></td>
					<td><?=$row1["dosage"]; ?></td>
					<td><?php
					
					$sql = Query("SELECT * FROM doctor WHERE docid =".$row1["docid"]."");
					$doc = fetchAssoc($sql);


					echo $doc["name"]; ?></td>
					<td><?php
					
					$sql = Query("SELECT * FROM patient WHERE patid =".$row1["patid"]."");
					$pat = fetchAssoc($sql);


					echo $pat["fname"]." ".$pat["lname"]; ?></td>
					<td><?=$row1["note"]; ?></td>
					<td> <div align="center">
					
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
							<span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li>
									<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Treatments&edit=<?=$row1["id"]; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
								</li>
								<li>
									<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Treatments&del=<?=$row1["id"]; ?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
								</li>
							</ul>
						</div>
					

			</div></td>
					
				</tr>
	<?php } ?>
			</tbody>
			</table>
	</div>
	<?php } else {
			$id = $_GET["edit"];
	
		if(isset($_POST["updateTreatment"])){
			$disease = $_POST["disease"];
			$treament= $_POST["treatment"];
			$dosage= $_POST["dosage"];
			$note = $_POST["note"];
			
			$sql = Query("UPDATE treatment  set disease   = '$disease',
			             						treatment = '$treament',
			             						note      = '$note',
											    dosage    = '$dosage'
										  
										  WHERE id        = $id
											");
			if($sql){
				$message = "Treatment updated";
			}else{
				$message = "Operation failed";
			}
		}

		
		$sql = Query("SELECT * FROM treatment WHERE id ='$id'");
		$row = fetchAssoc($sql);

?>
		<div class="panel-heading">Edit Treatment</div>
		<div class="panel-body">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
		<form method="POST" action="#">
			<div class="form-group">
					<label for="patient ID">Appointment Date-time:</label>
						<select disabled class="form-control" name="date">
							<option value="<?=$row["date"];?>"><?=$row["date"];?></option>
						</select>
			</div>
			<div class="form-group">
				<label class="control-label" for="disease">Disease:</label>
				<input required type="text" class="form-control" id="disease" value="<?=$row["disease"];?>" name="disease">
			</div>
			<div class="form-group">
				<label class="control-label" for="disease">Treatment:</label>
				<input required type="text" class="form-control" value="<?=$row["treatment"];?>" id="treatment" name="treatment">
			</div>
			<div class="form-group">
				<label class="control-label" for="disease">Dosage:</label>
				<input required type="text" class="form-control" value="<?=$row["dosage"];?>" id="dosage" name="dosage">
			</div>
			<div class="form-group">
					<label for="comment">Note:</label>
					<textarea required class="form-control" value="<?=$row["notes"];?>" rows="5" id="message" name="note"></textarea>
				</div>
			<div class="form-group">
				
					<button type="submit" class="btn btn-success" name="updateTreatment">Submit</button>
	
			</div>
		</form>

	<?php } ?>
</div> 			