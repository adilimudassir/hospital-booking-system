<?php
if(isset($_GET["del"])){
	if(Query("DELETE FROM doctor WHERE docid = ".$_GET["del"]."")){
		$message = "Doctor deleted";
	}
}
?>
<div class="panel panel-primary">
	<?php if(!isset($_GET["edit"])){ ?>
		<div class="panel-heading">Manage Doctors</div>
	<div class="panel-body">
		<div class="row" style="margin:5px;">
		<div class="col-sm-9">
			<?php if(isset($message)){ ?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?=$message;?></strong>
			</div>
			<?php } ?>
		</div>
		<div class="col-sm-3">
			<a class="btn btn-primary pull-right" href="<?=WEB_ROOT;?>view.php?mod=admin&view=AddDoctor">Add new Doctor</a>
		</div>
	</div>
	<div class="panel-body">  
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No.</th>
					<th>ID</th>
					<th>Name</th>
					<th>Sex</th>
					<th>Email</th>
			     	<th>Specialty</th>						
					<th>Department</th>
					<th>Phone</th>
					<th>Registration date</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
		<?php 
		$sql = Query("SELECT * FROM  doctor dc INNER JOIN departments dp ON dc.department = dp.dept_id");
		$n = 0;
		while($row1 = fetchAssoc($sql)){?>
				<tr>
					<td><?=++$n; ?></td>
					<td><?=$row1["docid"]; ?></td>
					<td><?=$row1["name"]; ?></td>
					<td><?=$row1["sex"]; ?></td>
					<td><?=$row1["email"]; ?></td>
					<td><?=$row1["speciality"]; ?></td>
					<td>
					<?php 
					
					// $dept= Query("SELECT * FROM departments WHERE dept_id=".$row1["department"]."");
					// $res=fetchAssoc($dept);
					echo $row1["dept_name"];
					?></td>
					<td><?=$row1["tel"]; ?></td>
					<td><?=date("d-m-Y", strtotime($row1["date"])); ?></td>	
					<td> <div align="center">
					
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
							<span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Doctors&del=<?=$row1["docid"]; ?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
									</li>
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=admin&view=Doctors&edit=<?=$row1["docid"]; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
									</li>
							</ul>
						</div>
					

			</div></td>
					
				</tr>
	<?php } ?>
			</tbody>
		</table>
		
	</div>
	<?php } else { 

		$id = $_GET["edit"];


		if(isset($_POST["updateDoctor"])){

			$name = $_POST["name"];
		    $dept = $_POST["department"];
		    $spec = $_POST["speciality"];
		    $email = $_POST["email"];
		    $tel = $_POST["tel"];
		    $sex = $_POST["sex"];


	    	$sql = Query("UPDATE doctor  set         name  = '$name',
				             					department = '$dept',
				             					speciality = '$spec',
				             						 email = '$email',
												       tel = '$tel',
												       sex = '$sex'
	 										  
										 WHERE      docid  = '$id'
												");
				if($sql){
					$message = "Doctor updated";
				}else{
					$message = "Operation failed";
				}
		}
		
		$sql = Query("SELECT * FROM doctor WHERE docid ='$id'");
		$row = fetchAssoc($sql);

		?>

	<div class="panel-heading">Edit Doctor</div>
	<div class="panel-body">
		<?php if(isset($message) and $message != ""){ ?>
        <div class="alert alert-info">
             <strong><?=$message; ?></strong>
        </div>
    <?php }  ?>
        <form class="form-horizontal" method="POST" action="">
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Name</label>
                <div class="col-md-10">
                    <input id="name" type="text" class="form-control" value="<?=$row["name"];?>" name="name" required autofocus />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Specialty</label>
                <div class="col-md-10">
                    <input id="speciality" type="text" class="form-control" value="<?=$row["speciality"];?>" name="speciality" required/>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-md-2 control-label">Department</label>
                <div class="col-md-10">
                    <select class="form-control" name="department" id="department">
                    	<?php
                    	$dept = $row["department"];
                    	 $dept = fetchAssoc(Query("SELECT * FROM departments WHERE dept_id = '$dept'"));
                    	  ?>
                        <option value="<?=$dept["dept_id"];?>" selected="selected"><?=$dept["dept_name"];?></option>   
                            <?php
                            $sql1 = Query("SELECT * FROM departments");
                            while($doc = fetchAssoc($sql1)){
                            ?>
                            <option value="<?php echo $doc["dept_id"]; ?>"><?php echo $doc["dept_name"]; ?></option>
                            <?php }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-md-2 control-label">E-Mail Address</label>
                <div class="col-md-10">
                    <input id="email" type="email" class="form-control" value="<?=$row["email"];?>" name="email" value="" required />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Sex</label>
                <div class="col-md-10">
                   <select required class="form-control"  name="sex" id="sex">
                   		<option value="<?=$row["sex"];?>"><?=$row["sex"];?></option>
                        <option selected="selected" value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">Phone Number</label>
                <div class="col-md-10">
                    <input id="tel" type="text" class="form-control" value="<?=$row["tel"];?>" name="tel" required/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <input type="submit" name="updateDoctor" value="Update" class="btn btn-success" />
                </div>
            </div>
        </form>
	</div>
	<?php } ?>
</div> 			