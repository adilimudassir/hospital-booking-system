<?php
if(isset($_GET['del']))
{$sql = Query("DELETE FROM appointment WHERE id = ".$_GET["del"]."");
if($sql){?>
        <script>
            window.alert("Appointment Deleted");
        </script>
  <?php } else { ?>
        <script>
            window.alert("Appointment could not be Deleted");
        </script>
      <?php }}
?>
<div class="panel panel-primary">
	<div class="panel-heading">My Appointments</div>
	<div class="panel-body">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>App.No</th>
					<th>Date Time</th>
					<th>Appointment<br />Date</th>
					<th>Appointment <br /> Time</th>
					<th>Doctor Name</th>
					<th>Complaints</th>
					<th>Status</th>
					<th>Options</th>
				</tr>
			</thead>

			<tbody>
			<?php 
			$sql = Query("SELECT * FROM  appointment WHERE patid =".$_SESSION['patid']."");
			$n = 0;
			while($row1 = fetchAssoc($sql)){
			$n++;
			?>
				<tr>
					<td><?=$n; ?></td>
					<td><?=date("d-m-Y h:i:s", strtotime($row1["date"])); ?></td>
					<td><?=date("d-m-Y", strtotime($row1["adate"])); ?></td>
					<td><?=$row1["atime"]; ?></td>
					<td><?php

					$result= Query("SELECT * FROM doctor WHERE docid=".$row1["docid"]."");
					$row = fetchArray($result);
						echo $row["name"];
					?></td>
					<td><?=$row1["comment"]; ?></td>
					<td><?=$row1["status"]; ?></td>
					<td> 
						<div align="center">
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=patient&view=ViewAppointments&del=<?=$row1["id"];?>"><span class="glyphicon glyphicon-trash"></span> Delete</a>
									</li>
									
									<li>
										<a href="<?=WEB_ROOT;?>view.php?mod=patient&view=Compose&id=<?=$row["docid"]; ?>"><span class="glyphicon glyphicon-envelope"></span> Message Doctor</a>
									</li>
								</ul>
							</div>
						</div>
					</td>
				</tr>
	<?php } ?>
				</tbody>
			</table>
	</div>
</div> 			