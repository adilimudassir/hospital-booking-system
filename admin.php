<div class="panel panel-primary">
	<div class="panel-heading">Current Site Statistics </div>
	<div class="panel-body">
		<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>Department</th>
						<th>Doctors</th>
						<th>Patients</th>
						<th>Booked Appointments</th>
						<th>Commenced Appointments</th>
						<th>Pending Appointments</th>
						<th>Treatments</th>
					</tr>
				</thead>
	
				<tbody>
	<?php 
	$sql = Query("SELECT * FROM  department");
	while($dept = fetchAssoc($sql)){
	$id = $dept["id"]
	?>
					<tr>
						<td><?=$dept["id"]; ?></td>
						<td><?=$dept["name"]; ?></td>
						<td>
						<?php
						$result= Query("SELECT * FROM doctor WHERE department='$id'");
						$doc = numRows($result);
							echo $doc;
						?></td>
						<td>
						<?php
						$result= Query("SELECT * FROM patient WHERE department='$id'");
						$doc = numRows($result);
							echo $doc;
						?>
						</td>
						<td><?php
						$result= Query("SELECT * FROM appointment WHERE department='$id'");
						$doc = numRows($result);
							echo $doc;
						?></td>
						<td>
						<?php
						$result= Query("SELECT * FROM appointment WHERE department ='$id' AND status = 'Commenced'");
						$doc = numRows($result);
							echo $doc;
						?>
						</td>
						<td>
						<?php
						$result= Query("SELECT * FROM appointment WHERE department='$id' AND happen='no' AND status = 'pending'");
						$doc = numRows($result);
							echo $doc;
						?>
						</td>
						<td>
						<?php
						$result= Query("SELECT * FROM treatment WHERE department='$id'");
						$doc = numRows($result);
							echo $doc;
						?>
						</td>
						
						
					</tr>
	<?php } ?>
				</tbody>
			</table>
	</div>
</div> 			