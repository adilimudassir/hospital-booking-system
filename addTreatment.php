<?php			
if(isset($_POST["submit"])){
	$query =Query("SELECT * FROM doctor WHERE docid=".$_SESSION["id"]."");
	$res = fetchAssoc($query);
	
	$appt = $_POST["appointment"];
	$disease = $_POST["disease"];
	$treament= $_POST["treatment"];
	$dosage= $_POST["dosage"];
	$note = $_POST["note"];
	$pid = $_POST["patient"];
	$docid = $_SESSION["id"];
	$dept = $res["department"];
	$sql = Query("INSERT INTO treatment (
									appt_id,
									patid,
									docid,
									disease,
									treatment,
									note,
									dosage,
									department
									)
							VALUES (
									'$appt',
									'$pid',
									'$docid',
									'$disease',
									'$treament',
									'$note',
									'$dosage',
									'$dept'
									)");
	if($sql){
		$message = "Treatment Added";
	}else{
		$message = "Operation failed";
	}
}
?>
<div class="panel panel-primary">
	<div class="panel-heading">Add New Treatment</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?=$message;?></strong>
		</div>
		<?php } ?>
		<form method="post" action="#">
			<h3 class="text-primary">Search Patient by ID</h3>
				<div class="input-group">
					<input required type="text" class="form-control" placeholder="Search" name="id">
					<div class="input-group-btn">
						<button class="btn btn-primary" type="submit" name="search">
							<span class="glyphicon glyphicon-search"></span>
						</button>
					</div>
				</div>
		</form>

	<?php
	if(isset($_POST["id"], $_POST["search"]) && $_POST["id"] != "") {
		$id = $_POST["id"];		
		$sql = Query("SELECT * FROM patient WHERE patid ='$id'");
		$row = fetchAssoc($sql);
		$patid = $id; 
	?>
	<div class="well row " style="margin:30px 0 30px 0;">
		<div class="col-md-3 text-primary" style="font-weight:bold;">
			Patient ID
		</div>
		<div class="col-md-9 clear" style="">
			<?=$row["patid"]; ?>
		</div>
		<div class="col-md-3 text-primary" style="font-weight:bold;">
			Patient Name
		</div>
		<div class="col-md-9 clear" style="">
			<?=$row["fname"]." ".$row["lname"]; ?>
		</div>
	</div>

	<form method="POST" action="" id="form" novalidate="novalidate">
		<h3 class="text-primary">Add Treatment</h3>
		<input type="hidden" class="form-control" name="patient" value="<?=$id;?>">
			<div class="form-group">
					<label for="patient ID">Appointment Date-time:</label>
						<select required class="form-control" name="appointment" id="appointment">
							<option value="">---SELECT---</option>
							<?php
							$sql = Query("SELECT * FROM appointment WHERE patid = ".$row["patid"]." AND docid = ".$_SESSION["id"]."");
							while($appt = fetchAssoc($sql)){
								?><option value="<?=$appt["id"]; ?>"><?="On ".$appt["adate"]." at ".$appt["atime"]; ?></option>
							<?php
								}
							?>
						</select>
			</div>
			<div class="form-group">
				<label class="control-label" for="disease">Disease:</label>
				<input required type="text" class="form-control" id="disease" name="disease">
			</div>
			<div class="form-group">
				<label class="control-label" for="disease">Treatment:</label>
				<input required type="text" class="form-control" id="treatment" name="treatment">
			</div>
			<div class="form-group">
				<label class="control-label" for="disease">Dosage:</label>
				<input required type="text" class="form-control" id="dosage" name="dosage">
			</div>
			<div class="form-group">
					<label for="comment">Note:</label>
					<textarea required class="form-control" rows="5" id="note" name="note"></textarea>
				</div>
			<div class="form-group">
				
					<input type="submit" class="btn btn-primary" value="Submit" name="submit" >
	
			</div>
		</form>
		<?php } ?>	


	</div>
</div> 			

<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                appointment : {
                   required  : true
                },
                disease : {
                    required : true,
                    minlength : 3
                },
                treatment : {
                    required : true,
                    minlength : 3
                },
                dosage : {
                    required : true,
                     minlength : 3
                },
                note : {
                    required : true,
                    minlength : 3
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>

