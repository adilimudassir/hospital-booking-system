<?php
require_once './library/config.php';
require_once './library/functions.php';

if(isset($_POST["submit"])){
	
	
	$_SESSION["docid"] = $_POST["docID"];
	$_SESSION["appdate"] = $_POST["appdate"];
	$_SESSION["comment"] = $_POST["comment"];
	$_SESSION["dept"] = $_SESSION["appDocDept"];
	$dept = $_SESSION["dept"];
	$appdate = $_POST["appdate"];
	$sql = Query("UPDATE patient SET department='$dept'");

	if($sql){
		$message = "Saved";	
		header("Location:appointmentTime.php?adate=$appdate");
	}

}
?>
<?php
require_once 'include/header.php';
require_once 'include/nav.php';

?>
<div class="row">
    
    <div class="col-sm-3">
      <?php require 'include/menu.php'; ?>
    </div>
    <div class="col-sm-9">
     	<div class="panel panel-primary">
	<div class="panel-heading">Make Appointment</div>
	
	<div class="panel-body">
		<?php if(isset($message)){ ?>
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?=$message;?></strong>
		</div>
		<?php } ?>
		<form role="form" method="post" action="#" id="form" novalidate="novalidate">
					<div class="form-group">
						<label for="patient ID">Patient ID:</label>
						<input type="text" class="form-control" id="patid" name="patid" readonly="readonly" value="<?=$_SESSION["patid"]; ?>">
					</div>
					<div class="form-group">
						<label for="email">Patient Name:</label>
						<input type="text" class="form-control" id="name" name="name" readonly="readonly" value="<?=$_SESSION["name"]; ?>">
					</div>
					<div class="form-group">
						<label for="doctor name">Doctor Name</label>
						<input type="hidden" class="form-control" id="docID" name="docID" readonly="readonly" value="<?=$$_SESSION["appDocID"]; ?>" >
						<input type="text" class="form-control" id="docName" name="docName" readonly="readonly" value="<?=$_SESSION["appDocName"]; ?>" >
					</div>
					<div class="form-group">
						<label for="pwd">Department:</label>
						<input type="text" class="form-control" id="department" name="department" readonly="readonly" value="<?php 
						$department = $_SESSION["appDocDept"];
						$dept= Query("SELECT * FROM departments WHERE dept_id='$department'");
						$res=fetchAssoc($dept);
						echo $res['dept_name'];
						
						?>">
					</div>
					
					<?php
						$tomorrow = mktime(0,0,0,date("m"),date("d")+2,date("Y"));
						$tdate= date("d/m/Y", $tomorrow);
						
					?>
					<div class="form-group">
						<input type="hidden" name="tomorrowDate" id="tomorrowDate" value="<?=$tdate;?>">
						<label for="Appointment Date">Appointment Date</label>
						
						 <div class="input-group date" id="datetimepicker" >
							<input required type="date" class="form-control" id="appdate" name="appdate" placeholder="yyyy/mm/dd">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calender"></span>
							</span>
						</div>
					</div>
					<div class="form-group">
						<label for="comment">Complaint:</label>
						<textarea required class="form-control" rows="5" id="comment" name="comment"></textarea>
					</div>
					<input type="submit" name="submit" id="submit" class="btn btn-success" value="Save" style="float:left;"> 
					
					
				</form>

	</div>
</div> 			

<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                
                appdate : {
                    required : true,
                    date   : true
                },
                comment : {
                    required : true,
                    minlength : 6
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>

    </div>

</div>

<?php

require_once 'include/footer.php'; 
?>
