<?php
if(isset($_POST["submit"])){
		
	$fname = $_POST["fname"];
	$lname = $_POST["lname"];
	$email = $_POST["email"];
	$tel   = $_POST["tel"];
	$sex   = $_POST["sex"];
	$id    = $_SESSION["patid"];
	
	$sql = Query("UPDATE patient SET fname='$fname', lname='$lname', email='$email', tel='$tel', sex='$sex' WHERE patid='$id'");
		if($sql){
			$message = "Account Updated";
		}else{
			$message = "Failed to update account";
		}
}
	$id = $_SESSION["patid"];
	$sql = Query("SELECT * FROM patient WHERE patid='$id'");
	$row = mysqli_fetch_assoc($sql);
?>
<div class="panel panel-primary">
	<div class="panel-heading">My Account</div>
	<div class="panel-body">
		<?php if(isset($message)){ ?>
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?=$message;?></strong>
		</div>
		<?php } ?>
		<form role="form" method="post" action="" id="form" novalidate="novalidate">
					<div class="form-group">
						<label for="email">First Name:</label>
						<input type="text" class="form-control" id="fname" name="fname" value="<?php echo $row["fname"];?>" placeholder="Enter First Name">
					</div>
					<div class="form-group">
						<label for="email">Last Name:</label>
						<input type="text" class="form-control" id="lname" name="lname" value="<?php echo $row["lname"];?>" placeholder="Enter Last Name">
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="email" class="form-control" id="email" value="<?php echo $row["email"];?>" name="email" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="Sex">Sex:</label>
						<select class="form-control" name="sex" id="sex">
						<option selected="selected" value="<?php echo $row["sex"];?>"><?php echo $row["sex"];?></option>
						<option value="Male">Male</option>
						<option value="Female">Female</option>
						</select>
					</div>
					<div class="form-group">
						<label for="tel">Contact Number:</label>
						<input type="text" class="form-control" value="<?php echo $row["tel"];?>" id="tel" name="tel" placeholder="Enter Contact">
					</div>
					<input type="submit" name="submit" class="btn btn-success" value="Update" style="float:left;"> 
					
					
				</form>
	</div>
</div> 			

<script type="text/javascript">
    $(document).ready(function() {
          var $validator = $("#form").validate({
            rules: {
                fname : {
                   required  : true,
                   minlength : 3
                },
                lname : {
                    required : true,
                    minlength : 3
                },
                email : {
                    required : true,
                    email    : true
                },
                sex : {
                    required : true
                },
                tel : {
                    required : true,
                    digits   : true,
                    minlength : 11,
                    maxlength : 11
                }
            },

            messages: {
            },     
            highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            }
          });

      })
</script>